package com.brst.aomnia.ui.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.aomnia.R;
import com.brst.aomnia.controller.ApiInterface;
import com.brst.aomnia.controller.RetrofitClient;
import com.brst.aomnia.pojo.ResponsePojo;
import com.brst.aomnia.pojo.UserRequestListing;
import com.brst.aomnia.ui.adapter.AllRequestResultAdapter;
import com.brst.aomnia.ui.baseactivities.BaseActivity;
import com.brst.aomnia.ui.fragments.AboutUsFragment;
import com.brst.aomnia.ui.fragments.FAQFragment;
import com.brst.aomnia.ui.fragments.HowItWorkFragment;
import com.brst.aomnia.ui.fragments.LoginFragment;
import com.brst.aomnia.ui.fragments.SearchVehiclesFragment;
import com.brst.aomnia.utilities.CallProgressWheel;
import com.brst.aomnia.utilities.MyApplication;
import com.brst.aomnia.utilities.MyUtility;
import com.brst.aomnia.utilities.PreferenceHandler;
import com.brst.aomnia.utilities.SpacesItemDecoration;
import com.brst.aomnia.utilities.Utilites;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by brst-pc97 on 1/22/18.
 */

public class RequestResultActivity extends BaseActivity {

    private CompositeDisposable mCompositeDisposable;
      Toolbar toolbar;
    TextView  text_toolbar_title;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView mRvRequest;
    AllRequestResultAdapter adapter;
    List<UserRequestListing> mList;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_result);

        mCompositeDisposable = new CompositeDisposable();
        setUpViews();

        setUpData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCompositeDisposable.dispose();
    }

    @Override
    public void setUpViews() {

        toolbar = findViewById(R.id.toolbar);

        text_toolbar_title = findViewById(R.id.text_toolbar_title);
        mRvRequest=findViewById(R.id.rv_request_Result);

        mLayoutManager = new LinearLayoutManager(this);
        mRvRequest.setLayoutManager(mLayoutManager);
        int spacingInPixels = 7;

        mRvRequest.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
    }

    @Override
    public void setUpData() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.back);
        text_toolbar_title.setText("Request details");


        if(Utilites.isInternetOn(MyApplication.mContext)){
           getData();
        }else {
            MyUtility.getInstance().showError(this, mRvRequest, "Please check your Internet connection");
        }

    }

    @Override
    public void onClick(View v) {
     switch (v.getId()){

         case R.id.btn_view_change:
//                 Utilites.showToastMessageShort(this, "In progress...");
             Intent I = new Intent(this, UpdateProfileActivity.class);
             startActivity(I);


             break;

     }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()){

            case android.R.id.home:
                onBackPressed();
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getData(){

        try {


            try {
                CallProgressWheel.showLoadingDialog(this, "Loading...");
                ApiInterface apiInterface = RetrofitClient.createService(ApiInterface.class);
                JsonObject object = new JsonObject();
                object.addProperty("userid",PreferenceHandler.readInteger(this,PreferenceHandler.PREF_KEY_USER_ID, -1));

                mCompositeDisposable.add(apiInterface.requestList(object)

                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(this::handleResponse,this::handleError));

            }catch (Exception e){
                e.printStackTrace();
                CallProgressWheel.dismissLoadingDialog();
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }



    private void handleResponse(ResponsePojo responsePojo) {

        CallProgressWheel.dismissLoadingDialog();
        if(responsePojo != null){

            if(responsePojo.getStatus_code() == 200) {

                if(responsePojo.getData() != null){


                    if(mList == null){
                        mList = new ArrayList<>();
                    }
                    mList.clear();

                    mList.addAll( responsePojo.getData().getUserRequestListing());

                    setAdapter();
                }
            }else if(responsePojo.getError() != null){

                Utilites.showToastMessageShort(MyApplication.mContext, responsePojo.getError().getDescription());
            }

        }
    }

    private void handleError(Throwable error) {

        CallProgressWheel.dismissLoadingDialog();
        Utilites.showToastMessageShort(MyApplication.mContext, Utilites.getErrorMessage(error, MyApplication.mContext));
    }

    private void  setAdapter(){

        try {

          if (adapter == null){

              adapter = new AllRequestResultAdapter(this, mList);
              mRvRequest.setAdapter(adapter);

          }else {

              adapter.notifyDataSetChanged();
          }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
//
}


