package com.brst.aomnia.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brst.aomnia.R;
import com.brst.aomnia.pojo.UserRequestListing;
import com.brst.aomnia.pojo.VehicleDatum;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by brst-pc97 on 1/23/18.
 */

public class AllRequestResultAdapter extends RecyclerView.Adapter<AllRequestResultAdapter.ViewHolder> {


    List<UserRequestListing> mList;
    Context mContext;



    public AllRequestResultAdapter(Context acontext, List<UserRequestListing> alist){

        mList= alist;
        mContext = acontext;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_request_result, parent, false);
        AllRequestResultAdapter.ViewHolder viewHolder = new AllRequestResultAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {

            holder.mTvName.setText("Name: "+mList.get(position).getName());
            holder.mTvEmail.setText("Email: "+mList.get(position).getEmail());
            holder.mTvMobile.setText("Mobile: "+mList.get(position).getMobile());
            holder.mTvDateCreated.setText(mList.get(position).getDateCreated());
            holder.mTvStatus.setText(mList.get(position).getStatus());
            holder.mTvVehicleName.setText("Vehicle Name: "+mList.get(position).getVehicle_name());


//
            Glide.with(mContext)
                    .load(mList.get(position).getImage())

                    .into(holder.mIvCar);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTvName , mTvEmail, mTvMobile, mTvDateCreated, mTvStatus, mTvVehicleName;
        ImageView mIvCar;

        public ViewHolder(View itemView) {
            super(itemView);

            mTvName = itemView.findViewById(R.id.text_view_name);
            mTvEmail = itemView.findViewById(R.id.text_view_email);
            mTvMobile = itemView.findViewById(R.id.text_view_mobile);
            mTvVehicleName = itemView.findViewById(R.id.text_view_vehicle_name);
            mTvDateCreated = itemView.findViewById(R.id.text_view_date_created);
            mTvStatus = itemView.findViewById(R.id.btn_view_status);


            mIvCar =itemView.findViewById(R.id.image_view_car);


        }
    }





}
