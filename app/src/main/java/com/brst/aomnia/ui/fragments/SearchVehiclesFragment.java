package com.brst.aomnia.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.brst.aomnia.R;
import com.brst.aomnia.controller.ApiInterface;
import com.brst.aomnia.controller.RetrofitClient;
import com.brst.aomnia.pojo.Color;
import com.brst.aomnia.pojo.DataPojo;
import com.brst.aomnia.pojo.Maker;
import com.brst.aomnia.pojo.Model;
import com.brst.aomnia.pojo.Package;
import com.brst.aomnia.pojo.ResponsePojo;
import com.brst.aomnia.ui.activities.HomeActivity;
import com.brst.aomnia.ui.adapter.ColorsAdapter;
import com.brst.aomnia.ui.adapter.LvlAdapter;
import com.brst.aomnia.ui.adapter.MakersAdapter;
import com.brst.aomnia.ui.adapter.ModelAdapter;
import com.brst.aomnia.ui.adapter.PackagesAdapter;
import com.brst.aomnia.utilities.CallProgressWheel;
import com.brst.aomnia.utilities.GlobalBus;
import com.brst.aomnia.utilities.MyApplication;
import com.brst.aomnia.utilities.MyUtility;
import com.brst.aomnia.utilities.PreferenceHandler;
import com.brst.aomnia.utilities.PushPopFragment;
import com.brst.aomnia.utilities.SpacesItemDecoration;
import com.brst.aomnia.utilities.Utilites;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SearchVehiclesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchVehiclesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    View view;

    // TODO: Rename and change types of parameters
    AppCompatSpinner mAsMakers, mAsModels, mAsColors, mAsPackages, mAsLvl;
    String[] arrayMakers, arrayModels, arrayColors, arrayPackages, arrayLvl;

    RecyclerView mRvMakers, mRvModels, mRvColors, mRvPackages, mRvLvl;
    LinearLayoutManager horizontalLayoutManagaer,horizontalLayoutManagaerModels,horizontalLayoutManagaerColors, horizontalLayoutManagaerPackages,horizontalLayoutManagaerLvl ;

    MakersAdapter adapter;
    ModelAdapter adapterModel;
    ColorsAdapter adapterColor;
    PackagesAdapter adapterPackages;
    LvlAdapter adapterLvl;

    List<String> makers , models, colors, packages, lvl;
    ArrayAdapter<String> spinnerArrayAdapter,spinnerArrayAdapter1,spinnerArrayAdapter2,spinnerArrayAdapter3, spinnerArrayAdapter4;

    FrameLayout mFlSearch, mFlReset;
    private CompositeDisposable mCompositeDisposable;

    private List<Maker> trimLvlPojo = null;

    private List<Maker> makersPojo = null;

    private List<Package> packagesPojo = null;

    private List<Color> colorPojo = null;

    private List<Model> modelsPojo = null;

    public  EventBus bus;

    public SearchVehiclesFragment() {
        // Required empty public constructor
    }


    public static SearchVehiclesFragment newInstance() {
        SearchVehiclesFragment fragment = new SearchVehiclesFragment();
        Bundle args = new Bundle();


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCompositeDisposable = new CompositeDisposable();
        bus = GlobalBus.getBus();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCompositeDisposable.dispose();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

//        if (view != null) {
//
//            ViewGroup parent = (ViewGroup) view.getParent();
//            if (parent != null) {
//                parent.removeView(view);
//            }
//        }
        if (view==null) {
            view = inflater.inflate(R.layout.fragment_home, container, false);

            makers = new ArrayList<>();
            setUpViews();
            setListner();

            if(Utilites.isInternetOn(getActivity())){
                getModelsData();
            }
//            setAdapter();
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onResume() {
        super.onResume();

        HomeActivity.toolbar.setTitle("Search your vehicle");
        HomeActivity.CURRENT_TAG = "search vehicles";
        bus.register(this);
    }


    @Override
    public void onPause() {
        super.onPause();

        bus.unregister(this);
    }

    private void setUpViews() {

        try {
            mAsMakers =  view.findViewById(R.id.et_choose_maker);
            mAsModels =  view.findViewById(R.id.et_choose_model);
            mAsPackages =  view.findViewById(R.id.et_choose_packages);
            mAsColors =  view.findViewById(R.id.et_choose_color);
            mAsLvl =  view.findViewById(R.id.et_choose_lvl);



            mFlSearch = view.findViewById(R.id.layout_search);
            mFlReset = view.findViewById(R.id.layout_reset);


            mRvMakers = view.findViewById(R.id.recycler_view_makers);
            mRvModels = view.findViewById(R.id.recycler_view_models);
            mRvColors = view.findViewById(R.id.recycler_view_colors);
            mRvPackages = view.findViewById(R.id.recycler_view_packages);
            mRvLvl = view.findViewById(R.id.recycler_view_trim);


            horizontalLayoutManagaer
                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

            horizontalLayoutManagaerModels
                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

            horizontalLayoutManagaerColors
                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

            horizontalLayoutManagaerPackages
                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

            horizontalLayoutManagaerLvl
                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

            mRvMakers.setLayoutManager(horizontalLayoutManagaer);
            mRvModels.setLayoutManager(horizontalLayoutManagaerModels);
            mRvColors.setLayoutManager(horizontalLayoutManagaerColors);
            mRvPackages.setLayoutManager(horizontalLayoutManagaerPackages);
            mRvLvl.setLayoutManager(horizontalLayoutManagaerLvl);

            int spacingInPixels = 4;

            mRvMakers.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
            mRvModels.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
            mRvColors.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
            mRvPackages.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
            mRvLvl.addItemDecoration(new SpacesItemDecoration(spacingInPixels));


        }catch (Exception e){

            e.printStackTrace();
        }
    }


    String result = "^^^^";
    private void setListner() {

        try {

            mAsMakers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    if( makers == null){
                        makers = new ArrayList<>();
                    }
                    if(i!= 0) {
                        makers.add(arrayMakers[i]);
                        HashSet<String> hashSet = new HashSet<String>();
                        hashSet.addAll(makers);
                        makers.clear();
                        makers.addAll(hashSet);

                        makersPojo.get(i-1).setSelected(true);


                        setAdapter();

                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });



            mAsModels.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    if( models == null){
                        models = new ArrayList<>();
                    }
                    if(i!= 0) {
                        models.add(arrayModels[i]);
                        HashSet<String> hashSet = new HashSet<String>();
                        hashSet.addAll(models);
                        models.clear();
                        models.addAll(hashSet);
                        modelsPojo.get(i-1).setSelected(true);

                        setAdapterModels();

                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            mAsColors.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    if( colors == null){
                        colors = new ArrayList<>();
                    }
                    if(i!= 0) {
                        colors.add(arrayColors[i]);
                        HashSet<String> hashSet = new HashSet<String>();
                        hashSet.addAll(colors);
                        colors.clear();
                        colors.addAll(hashSet);
                        colorPojo.get(i-1).setSelected(true);
                        setAdapterColors();

                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            mAsPackages.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    if( packages == null){
                        packages = new ArrayList<>();
                    }
                    if(i!= 0) {
                        packages.add(arrayPackages[i]);
                        HashSet<String> hashSet = new HashSet<String>();
                        hashSet.addAll(packages);
                        packages.clear();
                        packages.addAll(hashSet);
                        packagesPojo.get(i-1).setSelected(true);
                        setAdapterpackages();

                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });


            mAsLvl.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    if( lvl == null){
                        lvl = new ArrayList<>();
                    }
                    if(i!= 0) {
                        lvl.add(arrayLvl[i]);
                        HashSet<String> hashSet = new HashSet<String>();
                        hashSet.addAll(lvl);
                        lvl.clear();
                        lvl.addAll(hashSet);
                        trimLvlPojo.get(i-1).setSelected(true);
                        setAdapterLvl();

                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            mFlSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Utilites.showToastMessageShort(MyApplication.mContext, "In Progress...");

                    if(Utilites.isInternetOn(getActivity())){

                        String result = "^^^^";
                        StringBuilder search = setSearchdata();
                         result = search.toString();

                        if(result.equalsIgnoreCase("^^^^")){
                      Utilites.showToastMessageShort(getActivity(), "Please select atleast one value");
                        }else {

                            bus.postSticky(search.toString());
                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                            fragmentTransaction.replace(R.id.frame, SearchResultFragment.newInstance(), HomeActivity.CURRENT_TAG);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();
                        }
                    }else {
                        MyUtility.getInstance().showError(getActivity(),mFlSearch, "Please check your Internet connection");
                    }



                }
            });

            mFlReset.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    try {
//                        int size = makers.size();
//                        makers.clear();
//                        adapter.notifyItemRangeRemoved(0, size);
                    }catch (Exception e){

                    }



                    Utilites.showToastMessageShort(MyApplication.mContext, "In Progress...");
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void setAdapter() {

        try {
            // The number of Columns

            if(adapter == null) {
                adapter = new MakersAdapter(getActivity(), makers);
                mRvMakers.setAdapter(adapter);
            }else {
                adapter.notifyDataSetChanged();
                mRvMakers.setAdapter(adapter);
            }

//            setvisiblityRv();

            adapter.onItemClick(new MakersAdapter.clickListner() {
                @Override
                public void itemClick(int position) {


                    for (Maker maker : makersPojo){
                        if(makers.get(position).equals(maker.getName())){
                             int  m=  makersPojo.indexOf(maker);
                            makersPojo.get(m).setSelected(false);
                            break;
                        }
                    }

                    makers.remove(position);
                    if(makers.size() == 0){
                        mAsMakers.setAdapter(spinnerArrayAdapter);
                    }
                    setAdapter();

                }
            });

        } catch (Exception e)

        {
            e.printStackTrace();
        }


    }


    private void setAdapterModels() {

        try {
            // The number of Columns

            if(adapterModel == null) {
                adapterModel = new ModelAdapter(getActivity(), models);
                mRvModels.setAdapter(adapterModel);
            }else {
                adapterModel.notifyDataSetChanged();
                mRvModels.setAdapter(adapterModel);
            }

//            setvisiblityRvModels();

            adapterModel.onItemClick(new ModelAdapter.clickListner() {
                @Override
                public void itemClick(int position) {


                    for (Model maker : modelsPojo){
                        if(models.get(position).equals(maker.getPname())){
                            int  m=  modelsPojo.indexOf(maker);
                            modelsPojo.get(position).setSelected(false);
                            break;
                        }
                    }

                    models.remove(position);
                    if(models.size() == 0){
                        mAsModels.setAdapter(spinnerArrayAdapter1);
                    }
                    setAdapterModels();

                }
            });

        } catch (Exception e)

        {
            e.printStackTrace();
        }


    }


    private void setAdapterColors() {

        try {
            // The number of Columns
            if(adapterColor == null) {
                adapterColor = new ColorsAdapter(getActivity(), colors);
                mRvColors.setAdapter(adapterColor);
            }else {
                adapterColor.notifyDataSetChanged();
                mRvColors.setAdapter(adapterColor);
            }

//            setvisiblityColors();

            adapterColor.onItemClick(new ColorsAdapter.clickListner() {
                @Override
                public void itemClick(int position) {

                    for (Color maker : colorPojo){

                        if(colors.get(position).equals(maker.getCname())){
                            int  m=  colorPojo.indexOf(maker);
                            colorPojo.get(position).setSelected(false);
                            break;
                        }
                    }
                    colors.remove(position);

                    if(colors.size() == 0){
                        mAsColors.setAdapter(spinnerArrayAdapter2);
                    }
                    setAdapterColors();

                }
            });


        } catch (Exception e)

        {
            e.printStackTrace();
        }


    }


    private void setAdapterpackages() {

        try {
            // The number of Columns

            if(adapterPackages == null) {
                adapterPackages = new PackagesAdapter(getActivity(), packages);
                mRvPackages.setAdapter(adapterPackages);
            }else {
                adapterPackages.notifyDataSetChanged();
                mRvPackages.setAdapter(adapterPackages);
            }

//            setvisiblityPackages();

            adapterPackages.onItemClick(new PackagesAdapter.clickListner() {
                @Override
                public void itemClick(int position) {
                    for (Package maker : packagesPojo){

                        if(packages.get(position).equals(maker.getPname())){
                            int  m=  packagesPojo.indexOf(maker);
                            packagesPojo.get(position).setSelected(false);
                            break;
                        }
                    }
                    packages.remove(position);
                    packagesPojo.get(position).setSelected(false);
                    if(packages.size() == 0){
                        mAsPackages.setAdapter(spinnerArrayAdapter3);
                    }
                    setAdapterpackages();

                }
            });

        } catch (Exception e)

        {
            e.printStackTrace();
        }


    }


    private void setAdapterLvl() {

        try {
            // The number of Columns

            if(adapterLvl == null) {
                adapterLvl = new LvlAdapter(getActivity(), lvl);
                mRvLvl.setAdapter(adapterLvl);
            }else {
                adapterLvl.notifyDataSetChanged();
                mRvLvl.setAdapter(adapterLvl);
            }

//            setvisiblityPackages();

            adapterLvl.onItemClick(new LvlAdapter.clickListner() {
                @Override
                public void itemClick(int position) {


                    for (Maker maker : trimLvlPojo){
                        if(lvl.get(position).equals(maker.getName())){
                            int  m=  trimLvlPojo.indexOf(maker);
                            trimLvlPojo.get(position).setSelected(false);
                            break;
                        }
                    }

                    lvl.remove(position);

                    if(lvl.size() == 0){
                        mAsLvl.setAdapter(spinnerArrayAdapter4);
                    }
                    setAdapterLvl();

                }
            });

        } catch (Exception e)

        {
            e.printStackTrace();
        }


    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inf) {
        //  inf = getActivity().getMenuInflater();
//        inf.inflate(R.menu.all_charity_menu, menu);
        super.onCreateOptionsMenu(menu, inf);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        return true;
    }






    private void getModelsData(){


        try {
            CallProgressWheel.showLoadingDialog(getActivity(), "Loading...");
            ApiInterface apiInterface = RetrofitClient.createService(ApiInterface.class);

            mCompositeDisposable.add(apiInterface.getSearchListing()

                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleResponse,this::handleError));
        }catch (Exception e){
            e.printStackTrace();
            CallProgressWheel.dismissLoadingDialog();
        }

    }

    private void handleResponse(ResponsePojo responsePojo) {
        CallProgressWheel.dismissLoadingDialog();
        if(responsePojo != null){

            if(responsePojo.getStatus_code() == 200) {

               setDropDownData(responsePojo.getData());

            }else if(responsePojo.getStatus_code() == 400){
                Utilites.showToastMessageShort(MyApplication.mContext, responsePojo.getMessage());
            }

        }
    }

    private void handleError(Throwable error) {

        CallProgressWheel.dismissLoadingDialog();
        Utilites.showToastMessageShort(MyApplication.mContext, Utilites.getErrorMessage(error, MyApplication.mContext));
    }



    private void setDropDownData(DataPojo pojo){

        try {

            if(makersPojo == null){
                makersPojo = new ArrayList<>();

            }
               makersPojo = pojo.getMakers();


            if(modelsPojo == null){
                modelsPojo = new ArrayList<>();
            }
            modelsPojo = pojo.getModels();



            if(colorPojo == null){
                colorPojo = new ArrayList<>();
            }


            colorPojo = pojo.getColor();


            if(packagesPojo == null){
                packagesPojo = new ArrayList<>();
            }
            packagesPojo = pojo.getPackages();

            if(trimLvlPojo == null){
                trimLvlPojo = new ArrayList<>();
            }
            trimLvlPojo = pojo.getTrimLvl();


              for (int i = 0 ; i < pojo.getMakers().size(); i++){

                  if(arrayMakers ==null){
                      arrayMakers = new String[pojo.getMakers().size()+1];
                      arrayMakers[i] = "--select--";
                  }
                  arrayMakers[i+1] = pojo.getMakers().get(i).getName();

              }


            // Initializing an ArrayAdapter
            spinnerArrayAdapter = new ArrayAdapter<String>(
                    getActivity(), R.layout.spinner_item, arrayMakers);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

            mAsMakers.setAdapter(spinnerArrayAdapter);



            for (int i = 0 ; i < pojo.getModels().size(); i++){

                if(arrayModels ==null){
                    arrayModels = new String[ pojo.getModels().size()+1];
                    arrayModels[i]     = "--select--";
                }
                arrayModels[i+1] = pojo.getModels().get(i).getPname();

            }


            // Initializing an ArrayAdapter
            spinnerArrayAdapter1 = new ArrayAdapter<String>(
                    getActivity(), R.layout.spinner_item, arrayModels);
            spinnerArrayAdapter1.setDropDownViewResource(R.layout.spinner_item);

            mAsModels.setAdapter(spinnerArrayAdapter1);


            for (int i = 0 ; i < pojo.getColor().size(); i++){

                if(arrayColors ==null){
                    arrayColors = new String[pojo.getColor().size()+1];
                    arrayColors[i]  = "--select--";
                }
                arrayColors[i+1] = pojo.getColor().get(i).getCname();

            }


            // Initializing an ArrayAdapter
            spinnerArrayAdapter2 = new ArrayAdapter<String>(
                    getActivity(), R.layout.spinner_item, arrayColors);
            spinnerArrayAdapter2.setDropDownViewResource(R.layout.spinner_item);

            mAsColors.setAdapter(spinnerArrayAdapter2);


            for (int i = 0 ; i < pojo.getPackages().size(); i++){

                if(arrayPackages ==null){
                    arrayPackages = new String[pojo.getPackages().size()+1];
                    arrayPackages[i]    = "--select--";
                }
                arrayPackages[i+1] = pojo.getPackages().get(i).getPname();

            }



            // Initializing an ArrayAdapter
            spinnerArrayAdapter3 = new ArrayAdapter<String>(
                    getActivity(), R.layout.spinner_item, arrayPackages);
            spinnerArrayAdapter3.setDropDownViewResource(R.layout.spinner_item);

            mAsPackages.setAdapter(spinnerArrayAdapter3);



            for (int i = 0 ; i < pojo.getTrimLvl().size(); i++){

                if(arrayLvl ==null){
                    arrayLvl = new String[pojo.getTrimLvl().size()+1];
                    arrayLvl[i]    = "--select--";
                }
                arrayLvl[i+1] = pojo.getTrimLvl().get(i).getName();

            }



            // Initializing an ArrayAdapter
            spinnerArrayAdapter4 = new ArrayAdapter<String>(
                    getActivity(), R.layout.spinner_item, arrayLvl);
            spinnerArrayAdapter4.setDropDownViewResource(R.layout.spinner_item);

            mAsLvl.setAdapter(spinnerArrayAdapter4);

        }catch (Exception e){
            e.printStackTrace();
//            colors^packages^make^models^trimlvls
        }

    }


    private StringBuilder setSearchdata(){
        StringBuilder searchText =  new StringBuilder();

        try {
//            colors^packages^make^models^trimlvls     -- order search
                for ( Color obj : colorPojo){
                     if(obj.isSelected()){
                         searchText.append(obj.getId());
                         searchText.append(",");

                     }
                }

            searchText.append("^");
            for ( Package obj : packagesPojo){
                if(obj.isSelected()){
                    searchText.append(obj.getId());
                    searchText.append(",");

                }
            }


            searchText.append("^");
            for ( Maker obj : makersPojo){
                if(obj.isSelected()){
                    searchText.append(obj.getId());
                    searchText.append(",");

                }
            }

            searchText.append("^");
            for ( Model obj : modelsPojo){
                if(obj.isSelected()){
                    searchText.append(obj.getId());
                    searchText.append(",");

                }
            }

            searchText.append("^");
            for ( Maker obj : trimLvlPojo){
                if(obj.isSelected()){
                    searchText.append(obj.getId());
                    searchText.append(",");

                }
            }




        }catch (Exception e){
            e.printStackTrace();
        }

        return searchText;

    }


    @Subscribe
    public void get(String message) {

    }
}
