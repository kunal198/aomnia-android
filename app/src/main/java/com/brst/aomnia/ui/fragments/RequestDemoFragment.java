package com.brst.aomnia.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.brst.aomnia.R;
import com.brst.aomnia.controller.ApiInterface;
import com.brst.aomnia.controller.RetrofitClient;
import com.brst.aomnia.pojo.ResponsePojo;
import com.brst.aomnia.ui.activities.HomeActivity;
import com.brst.aomnia.utilities.CallProgressWheel;
import com.brst.aomnia.utilities.ImagePicker;
import com.brst.aomnia.utilities.MyApplication;
import com.brst.aomnia.utilities.MyUtility;
import com.brst.aomnia.utilities.PreferenceHandler;
import com.brst.aomnia.utilities.PushPopFragment;
import com.brst.aomnia.utilities.Utilites;
import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RequestDemoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RequestDemoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match

    View view;
    EditText mEdEmail, mEdName, mEdPhone, mEdComments;
    FrameLayout mFlSubmit, mFlUpload;
    TextView mTvFilename;
    private CompositeDisposable mCompositeDisposable;
    public RequestDemoFragment() {
        // Required empty public constructor
    }


    public static RequestDemoFragment newInstance() {
        RequestDemoFragment fragment = new RequestDemoFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//
//        if (view != null) {
//
//            ViewGroup parent = (ViewGroup) view.getParent();
//            if (parent != null) {
//                parent.removeView(view);
//            }
//        }
        if (view==null) {
            view = inflater.inflate(R.layout.fragment_request_demo, container, false);

            setUpViews();
            setListner();

        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onResume() {
        super.onResume();
        HomeActivity.toolbar.setTitle("Request a Demo");
        HomeActivity.CURRENT_TAG = "login_signup";


    }

    private void setUpViews() {

        try {
            mEdEmail = view.findViewById(R.id.et_email);
            mEdPhone = view.findViewById(R.id.et_phone);

            mEdName = view.findViewById(R.id.et_name);
            mEdComments = view.findViewById(R.id.et_comments);
            mFlSubmit = view.findViewById(R.id.layout_submit);
            mTvFilename = view.findViewById(R.id.file_name);
            mFlUpload = view.findViewById(R.id.layout_upload);



        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void setListner() {

        try {

            mFlSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(validation()){
                        if(Utilites.isInternetOn(MyApplication.mContext)){
                           uploadFile();
                        }else {
                            MyUtility.getInstance().showError(getContext(), mEdEmail, "Please check your Internet connection");
                        }

                    }
                }
            });


            mFlUpload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onPickImage();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inf) {

        super.onCreateOptionsMenu(menu, inf);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return true;
    }


    private boolean validation(){

        if(Utilites.validationInput(mEdName)){

            MyUtility.getInstance().showError(getContext(), mEdName, "Please enter Name");
            return false;
        }


        if(Utilites.validationInput(mEdEmail)){

            MyUtility.getInstance().showError(getContext(), mEdEmail, "Please enter email");
            return false;
        }


        if(!Utilites.isValidEmailEnter(mEdEmail.getText().toString().trim())){
            MyUtility.getInstance().showError(getContext(), mEdEmail, "Please enter valid email");
            return false;
        }

        if(Utilites.validationInput(mEdComments)){
            MyUtility.getInstance().showError(getContext(), mEdComments, "Please enter comments");
            return false;
        }

        if(file == null){
            MyUtility.getInstance().showError(getContext(), mEdEmail, "Please select file");
        }


        return true;
    }



    //Image Picker
    public void onPickImage() {
        try {
            Intent chooseImageIntent = ImagePicker.getPickImageIntent(getActivity());
            startActivityForResult(chooseImageIntent, 234);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    File file;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            Uri selectedImageURI = null;
            if (data != null) {
                selectedImageURI = data.getData();
            }
            if (selectedImageURI != null) {


                String path = Utilites.getRealPathFromURI(getActivity(), selectedImageURI);

                file = new File(path);

                BitmapFactory.Options opts = new BitmapFactory.Options ();
                opts.inSampleSize = 2;   // for 1/2 the image to be loaded

                mTvFilename.setText(file.getName());

//                uploadFile();
            } else {

                Bitmap bitmap = ImagePicker.getImageFromResult(getActivity(), resultCode, data);
                if (bitmap != null) {
//                    file = ImagePicker.getTempFile(EditProfileActivity.this);
// CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                    Uri tempUri = getImageUri(getActivity(), bitmap);

                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    file = new File(getRealPathFromURI(tempUri));
                       mTvFilename.setText(file.getName());
//                    uploadFile();
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    void uploadFile() {
        try {
            CallProgressWheel.showLoadingDialog(getActivity(), "Loading...");
//            File file = new File(mImageName);

            RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("licence", file.getName(), mFile);

            RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());


            int userId = PreferenceHandler.readInteger(getActivity(), PreferenceHandler.PREF_KEY_USER_ID, -1);

            RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), userId+"");

            String name = mEdName.getText().toString().trim();

            RequestBody namerequest = RequestBody.create(MediaType.parse("text/plain"), name);

            String email = mEdEmail.getText().toString().trim();

            RequestBody emailrequest = RequestBody.create(MediaType.parse("text/plain"), email);


            String phone = mEdPhone.getText().toString().trim();

            RequestBody phonerequest = RequestBody.create(MediaType.parse("text/plain"), phone);

            String comments = mEdComments.getText().toString().trim();

            RequestBody commentsrequest = RequestBody.create(MediaType.parse("text/plain"), comments);


            int vID = PreferenceHandler.readInteger(getActivity(),PreferenceHandler.PREF_KEY_VEHICLE_ID, -1);

            RequestBody vehId = RequestBody.create(MediaType.parse("text/plain"), vID+"");

            ApiInterface apiInterface = RetrofitClient.createService(ApiInterface.class);

            mCompositeDisposable.add(apiInterface.uploadFile(fileToUpload, user_id,namerequest, emailrequest, phonerequest, commentsrequest, vehId)

                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleResponse,this::handleError));


        } catch (Exception e)
        {
            e.printStackTrace();
            CallProgressWheel.dismissLoadingDialog();
        }
    }


    private void handleResponse(ResponsePojo responsePojo) {

        CallProgressWheel.dismissLoadingDialog();
        if(responsePojo != null){

            if(responsePojo.getStatus_code() == 200) {
                Utilites.showToastMessageShort(MyApplication.mContext, responsePojo.getMessage());


                mEdName.setText("");
                mEdEmail.setText("");
                mEdComments.setText("");
                mEdPhone.setText("");
                file = null;
                mTvFilename.setText("");
            }else if(responsePojo.getError() != null){
                Utilites.showToastMessageShort(MyApplication.mContext, responsePojo.getError().getDescription());
            }

        }
    }

    private void handleError(Throwable error) {

        CallProgressWheel.dismissLoadingDialog();
        Utilites.showToastMessageShort(MyApplication.mContext, Utilites.getErrorMessage(error, MyApplication.mContext));
    }


}
