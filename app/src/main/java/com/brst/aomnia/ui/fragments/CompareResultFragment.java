package com.brst.aomnia.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brst.aomnia.R;
import com.brst.aomnia.controller.ApiInterface;
import com.brst.aomnia.controller.RetrofitClient;
import com.brst.aomnia.pojo.CompareIds;
import com.brst.aomnia.pojo.DataPojo;
import com.brst.aomnia.pojo.ResponsePojo;
import com.brst.aomnia.pojo.VehicleCompareDatum;
import com.brst.aomnia.pojo.VehicleDatum;
import com.brst.aomnia.ui.activities.HomeActivity;
import com.brst.aomnia.ui.adapter.CompareDescriptionAdapter;
import com.brst.aomnia.ui.adapter.SearchResultAdapter;
import com.brst.aomnia.utilities.CallProgressWheel;
import com.brst.aomnia.utilities.GlobalBus;
import com.brst.aomnia.utilities.MyApplication;
import com.brst.aomnia.utilities.MyUtility;
import com.brst.aomnia.utilities.SpacesItemDecoration;
import com.brst.aomnia.utilities.Utilites;
import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by brst-pc97 on 1/23/18.
 */

public class CompareResultFragment extends Fragment implements View.OnClickListener{


    TabLayout tabLayoutDetail;
    ViewPager viewPagerDetail;
    Context context;
    Button mBtnRequestDemo1, mBtnRequestDemo2, mBtnRequestDemo3;

    List<VehicleCompareDatum> listVehicle;
    private CompositeDisposable mCompositeDisposable;
    public EventBus bus;

    ImageView carImage1, carImage2, carImage3;
    TextView mcarName1, mcarname2, mCarName3, carPrice1, carprice2, carprice3;
    TextView mCarDesc1, mCarDesc2, mCarDesc3;

    LinearLayout mLi1, mLi2, mLi3;
    DataPojo pojo;
    String carId;

    public static CompareResultFragment newInstance() {
        CompareResultFragment fragment = new CompareResultFragment();
        Bundle args = new Bundle();
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCompositeDisposable = new CompositeDisposable();
        bus = GlobalBus.getBus();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_compare_vehicle, container, false);

        setUpViews(rootView);

        setListner();



        return rootView;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    private void setUpViews(View view){

        tabLayoutDetail = view.findViewById(R.id.tabLayoutDetail);
        viewPagerDetail = view.findViewById(R.id.viewPagerDetail);

        mBtnRequestDemo1 = view.findViewById(R.id.btn_view_details1);
        mBtnRequestDemo2 = view.findViewById(R.id.btn_view_details2);
        mBtnRequestDemo3 = view.findViewById(R.id.btn_view_details3);

        carImage1 = view.findViewById(R.id.image_car1);
        carImage2 = view.findViewById(R.id.image_car2);
        carImage3 = view.findViewById(R.id.image_car3);

        mcarName1 = view.findViewById(R.id.text_view_car_name1);
        mcarname2 = view.findViewById(R.id.text_view_car_name2);
        mCarName3 = view.findViewById(R.id.text_view_car_name3);

        carPrice1 = view.findViewById(R.id.text_view_price1);
        carprice2 = view.findViewById(R.id.text_view_price2);
        carprice3 = view.findViewById(R.id.text_view_price3);

        mCarDesc1 = view.findViewById(R.id.text_view_vehicle_desc1);
        mCarDesc2 = view.findViewById(R.id.text_view_vehicle_desc2);
        mCarDesc3 = view.findViewById(R.id.text_view_vehicle_desc3);


        mLi1 = view.findViewById(R.id.layout_image1);
        mLi2 = view.findViewById(R.id.layout_image2);
        mLi3 = view.findViewById(R.id.layout_image3);

    }
    private void setListner(){

        mBtnRequestDemo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bus.postSticky(pojo.getVpackages().get(0).getVehicleid());
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.replace(R.id.frame, CompareSingleVehicleFragment.newInstance(), HomeActivity.CURRENT_TAG);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        mBtnRequestDemo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bus.postSticky(pojo.getVpackages().get(1).getVehicleid());
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.replace(R.id.frame, CompareSingleVehicleFragment.newInstance(), HomeActivity.CURRENT_TAG);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        mBtnRequestDemo3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bus.postSticky(pojo.getVpackages().get(2).getVehicleid());
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.replace(R.id.frame, CompareSingleVehicleFragment.newInstance(), HomeActivity.CURRENT_TAG);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });


    }

    @Override
    public void onPause() {
        super.onPause();

        bus.unregister(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCompositeDisposable.dispose();
    }

    @Override
    public void onResume() {
        super.onResume();
        HomeActivity.toolbar.setTitle("Compare Vehicles");
        HomeActivity.CURRENT_TAG = "";
        bus.register(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

        }
    }

    private void setViewPagerAdaptor() {


        CompareDescriptionAdapter viewPagerAdaptor = new CompareDescriptionAdapter(context, getChildFragmentManager(), 3, pojo );

        // Set the adapter onto the view pager
        viewPagerDetail.setAdapter(viewPagerAdaptor);
        tabLayoutDetail.setupWithViewPager(viewPagerDetail);



    }


    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void getId(CompareIds s) {
//        Utilites.showToastMessageShort(getActivity(), s.getCompareIds());
        if(Utilites.isInternetOn(MyApplication.mContext)){
            carId = s.getCompareIds();
            if(carId == null);
            getData(s.getCompareIds());
        }else {
            MyUtility.getInstance().showError(getContext(), viewPagerDetail, "Please check your Internet connection");
        }

    }


    private void getData(String search){

        try {

            search= search.substring(0, search.length() - 1);
            CallProgressWheel.showLoadingDialog(getActivity(), "Loading...");
            ApiInterface apiInterface = RetrofitClient.createService(ApiInterface.class);
            JsonObject object = new JsonObject();
            object.addProperty("vehicleids",search);

            mCompositeDisposable.add(apiInterface.compareVehicles(object)

                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleResponse,this::handleError));

        }catch (Exception e){
            e.printStackTrace();
            CallProgressWheel.dismissLoadingDialog();
        }


    }

    private void handleResponse(ResponsePojo responsePojo) {

        CallProgressWheel.dismissLoadingDialog();
        if(responsePojo != null){

            if(responsePojo.getStatus_code() == 200) {

                if(responsePojo.getData() != null){

                    if (responsePojo.getData().getVpackages() != null){

                        if(listVehicle == null){
                            listVehicle = new ArrayList<>();
                        }

                        pojo = responsePojo.getData();
                        listVehicle = responsePojo.getData().getVpackages();
                        setViewData();
                    }
                }
            }else if(responsePojo.getStatus_code() == 400){
                Utilites.showToastMessageShort(MyApplication.mContext, responsePojo.getMessage());
            }

        }
    }

    private void handleError(Throwable error) {

        CallProgressWheel.dismissLoadingDialog();
        Utilites.showToastMessageShort(MyApplication.mContext, Utilites.getErrorMessage(error, MyApplication.mContext));
    }



    private void setViewData(){

        try {

            if(listVehicle.size() == 0){

                mLi1.setVisibility(View.GONE);
                mLi2.setVisibility(View.GONE);
                mLi3.setVisibility(View.GONE);
            }

            if(listVehicle.size() == 1){

                mLi1.setVisibility(View.VISIBLE);
                mLi2.setVisibility(View.GONE);
                mLi3.setVisibility(View.GONE);


                Glide.with(getActivity())
                        .load(listVehicle.get(0).getImage())


                        .into(carImage1);


                mcarName1.setText(listVehicle.get(0).getVname());
                carPrice1.setText("$"+listVehicle.get(0).getPrice()+"");



                mCarDesc1.setText(listVehicle.get(0).getVmodel());

            }


            if(listVehicle.size() == 2){

                mLi1.setVisibility(View.VISIBLE);
                mLi2.setVisibility(View.VISIBLE);
                mLi3.setVisibility(View.GONE);

                Glide.with(getActivity())
                        .load(listVehicle.get(0).getImage())
                        .into(carImage1);

                mcarName1.setText(listVehicle.get(0).getVname());
                carPrice1.setText("$"+listVehicle.get(0).getPrice()+"");

                StringBuilder temp = new StringBuilder();

                if (!listVehicle.get(0).getMileage().equalsIgnoreCase("")){
                    temp.append(listVehicle.get(0).getMileage());
                }

                if (!listVehicle.get(0).getFueltype().equalsIgnoreCase("")){
                    temp.append(", ");
                    temp.append(listVehicle.get(0).getFueltype());
                }

                if (!listVehicle.get(0).getDisplacement().equalsIgnoreCase("")){
                    temp.append(", ");
                    temp.append(listVehicle.get(0).getDisplacement());
                }

                mCarDesc1.setText(temp);



                Glide.with(getActivity())
                        .load(listVehicle.get(1).getImage())


                        .into(carImage2);


                mcarname2.setText(listVehicle.get(1).getVname());
                carprice2.setText("$"+listVehicle.get(1).getPrice()+"");


                temp = new StringBuilder();

                if (!listVehicle.get(1).getMileage().equalsIgnoreCase("")){
                    temp.append(listVehicle.get(1).getMileage());
                }

                if (!listVehicle.get(1).getFueltype().equalsIgnoreCase("")){
                    temp.append(", ");
                    temp.append(listVehicle.get(1).getFueltype());
                }

                if (!listVehicle.get(1).getDisplacement().equalsIgnoreCase("")){
                    temp.append(", ");
                    temp.append(listVehicle.get(1).getDisplacement());
                }

                mCarDesc2.setText(temp);
            }

            if(listVehicle.size() == 3){

                mLi1.setVisibility(View.VISIBLE);
                mLi2.setVisibility(View.VISIBLE);
                mLi3.setVisibility(View.VISIBLE);

                Glide.with(getActivity())
                        .load(listVehicle.get(0).getImage())


                        .into(carImage1);


                mcarName1.setText(listVehicle.get(0).getVname());
                carPrice1.setText("$"+listVehicle.get(0).getPrice()+"");

                StringBuilder temp = new StringBuilder();

                if (!listVehicle.get(0).getMileage().equalsIgnoreCase("")){
                    temp.append(listVehicle.get(0).getMileage());
                }

                if (!listVehicle.get(0).getFueltype().equalsIgnoreCase("")){
                    temp.append(", ");
                    temp.append(listVehicle.get(0).getFueltype());
                }

                if (!listVehicle.get(0).getDisplacement().equalsIgnoreCase("")){
                    temp.append(", ");
                    temp.append(listVehicle.get(0).getDisplacement());
                }

                mCarDesc1.setText(temp);



                Glide.with(getActivity())
                        .load(listVehicle.get(1).getImage())


                        .into(carImage2);


                mcarname2.setText(listVehicle.get(1).getVname());
                carprice2.setText("$"+listVehicle.get(1).getPrice()+"");


                temp = new StringBuilder();

                if (!listVehicle.get(1).getMileage().equalsIgnoreCase("")){
                    temp.append(listVehicle.get(1).getMileage());
                }

                if (!listVehicle.get(1).getFueltype().equalsIgnoreCase("")){
                    temp.append(", ");
                    temp.append(listVehicle.get(1).getFueltype());
                }

                if (!listVehicle.get(1).getDisplacement().equalsIgnoreCase("")){
                    temp.append(", ");
                    temp.append(listVehicle.get(1).getDisplacement());
                }

                mCarDesc2.setText(temp);


                Glide.with(getActivity())
                        .load(listVehicle.get(2).getImage())


                        .into(carImage3);


                mCarName3.setText(listVehicle.get(2).getVname());
                carprice3.setText("$"+listVehicle.get(2).getPrice()+"");

                temp = new StringBuilder();

                if (!listVehicle.get(2).getMileage().equalsIgnoreCase("")){
                    temp.append(listVehicle.get(2).getMileage());
                }

                if (!listVehicle.get(2).getFueltype().equalsIgnoreCase("")){
                    temp.append(", ");
                    temp.append(listVehicle.get(2).getFueltype());
                }

                if (!listVehicle.get(2).getDisplacement().equalsIgnoreCase("")){
                    temp.append(", ");
                    temp.append(listVehicle.get(2).getDisplacement());
                }

                mCarDesc3.setText(temp);


            }

            setViewPagerAdaptor();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
