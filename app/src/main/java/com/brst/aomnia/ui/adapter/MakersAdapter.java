package com.brst.aomnia.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.brst.aomnia.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brst-pc97 on 1/4/18.
 */

public class MakersAdapter extends RecyclerView.Adapter<MakersAdapter.ViewHolder> {

    ArrayList<String> alName;

    Context context;
    List<String> mList;
    clickListner listner;

    public MakersAdapter(Context context, List<String> alist) {
        super();
        this.context = context;
        this.alName = alName;
        this.mList = alist;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_makers, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        viewHolder.mTvMakers.setText(mList.get(i));


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder  {

        LinearLayout linearLayoutCategory;

        TextView mTvMakers;
        public ViewHolder(View itemView) {
            super(itemView);
           mTvMakers = itemView.findViewById(R.id.text_view_makers);

           itemView.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   if(listner != null){
                       listner.itemClick(getAdapterPosition());
                   }
               }
           });
        }


    }


    public  void onItemClick(clickListner aListner){
        this.listner = aListner;
    }

    public  interface clickListner{

        public  void itemClick(int position);

    }
}
