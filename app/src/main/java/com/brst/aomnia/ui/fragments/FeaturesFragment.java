package com.brst.aomnia.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.brst.aomnia.R;
import com.brst.aomnia.pojo.DataPojo;
import com.brst.aomnia.ui.adapter.FeaturesAdapter;
import com.brst.aomnia.utilities.SpacesItemDecoration;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FeaturesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FeaturesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    View view;

    private OnFragmentInteractionListener mListener;
    RecyclerView mRvCar1, mRvCar2, mRvCar3;

    FeaturesAdapter adapter;


    static DataPojo pojo;
    public FeaturesFragment() {
        // Required empty public constructor
    }



    public static FeaturesFragment newInstance(DataPojo aPojo) {
        FeaturesFragment fragment = new FeaturesFragment();
        pojo = aPojo;
        Bundle args = new Bundle();


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

//        if (view != null) {
//
//            ViewGroup parent = (ViewGroup) view.getParent();
//            if (parent != null) {
//                parent.removeView(view);
//            }
//        }
        if (view==null) {
            view = inflater.inflate(R.layout.fragment_features, container, false);

            setUpViews();
            setListner();

        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onResume() {
        super.onResume();



    }

    private void setUpViews() {

        try {

            mRvCar1 =  view.findViewById(R.id.recycler_view_car1);
            mRvCar2 =  view.findViewById(R.id.recycler_view_car2);
            mRvCar3 =  view.findViewById(R.id.recycler_view_car3);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            mRvCar1.setLayoutManager(mLayoutManager);
            int spacingInPixels = 7;

            mRvCar1.addItemDecoration(new SpacesItemDecoration(spacingInPixels));


            RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getContext());
            mRvCar2.setLayoutManager(mLayoutManager1);


            mRvCar2.addItemDecoration(new SpacesItemDecoration(spacingInPixels));


            RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(getContext());
            mRvCar3.setLayoutManager(mLayoutManager2);


            mRvCar3.addItemDecoration(new SpacesItemDecoration(spacingInPixels));


        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private void setListner() {

        try {

            setAdapter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inf) {
        //  inf = getActivity().getMenuInflater();
//        inf.inflate(R.menu.all_charity_menu, menu);
        super.onCreateOptionsMenu(menu, inf);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {




        return true;
    }


    private void setAdapter(){

            if(pojo.getVpackages().size() == 1){

                adapter = new FeaturesAdapter(getActivity(), pojo.getVpackages().get(0).getComparePackages());
                mRvCar1.setAdapter(adapter);

                mRvCar1.setVisibility(View.VISIBLE);
                mRvCar2.setVisibility(View.GONE);
                mRvCar3.setVisibility(View.GONE);



            }else if(pojo.getVpackages().size() == 2){
                adapter = new FeaturesAdapter(getActivity(), pojo.getVpackages().get(0).getComparePackages());
                mRvCar1.setAdapter(adapter);

                mRvCar1.setVisibility(View.VISIBLE);
                mRvCar2.setVisibility(View.VISIBLE);

                adapter = new FeaturesAdapter(getActivity(), pojo.getVpackages().get(1).getComparePackages());
                mRvCar2.setAdapter(adapter);


                mRvCar3.setVisibility(View.GONE);

            }else if(pojo.getVpackages().size() == 3){

                adapter = new FeaturesAdapter(getActivity(), pojo.getVpackages().get(0).getComparePackages());
                mRvCar1.setAdapter(adapter);

                mRvCar1.setVisibility(View.VISIBLE);
                mRvCar2.setVisibility(View.VISIBLE);

                adapter = new FeaturesAdapter(getActivity(), pojo.getVpackages().get(1).getComparePackages());
                mRvCar2.setAdapter(adapter);


                mRvCar3.setVisibility(View.VISIBLE);
                adapter = new FeaturesAdapter(getActivity(), pojo.getVpackages().get(2).getComparePackages());
                mRvCar3.setAdapter(adapter);

            }






    }

}
