package com.brst.aomnia.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.aomnia.R;
import com.brst.aomnia.controller.ApiInterface;
import com.brst.aomnia.controller.RetrofitClient;
import com.brst.aomnia.pojo.Android;
import com.brst.aomnia.pojo.ResponsePojo;
import com.brst.aomnia.ui.activities.ForgotPasswordActivity;
import com.brst.aomnia.ui.activities.HomeActivity;
import com.brst.aomnia.utilities.CallProgressWheel;
import com.brst.aomnia.utilities.MyApplication;
import com.brst.aomnia.utilities.MyUtility;
import com.brst.aomnia.utilities.PreferenceHandler;
import com.brst.aomnia.utilities.PushPopFragment;
import com.brst.aomnia.utilities.Utilites;
import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    View view;
    EditText mEdEmail, mEdPassword;
    FrameLayout mFlSignin;
    TextView mTvForgotPassword, mTvSignUp;
    private CompositeDisposable mCompositeDisposable;
    private OnFragmentInteractionListener mListener;

    public LoginFragment() {
        // Required empty public constructor
    }


    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

//        if (view != null) {
//
//            ViewGroup parent = (ViewGroup) view.getParent();
//            if (parent != null) {
//                parent.removeView(view);
//            }
//        }
        if (view==null) {
            view = inflater.inflate(R.layout.fragment_login, container, false);

            setUpViews();
            setListner();

        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onResume() {
        super.onResume();
        HomeActivity.toolbar.setTitle("Login");

        HomeActivity.CURRENT_TAG = "login_signup";
    }

    private void setUpViews() {

           try {
               mEdEmail = view.findViewById(R.id.et_email);
               mEdPassword = view.findViewById(R.id.et_password);
               mFlSignin = view.findViewById(R.id.layout_signin);
               mTvForgotPassword = view.findViewById(R.id.text_forgot_password);
               mTvSignUp = view.findViewById(R.id.text_view_sign_up);

           }catch (Exception e){
               e.printStackTrace();
           }


    }

    private void setListner() {

        try {
            mFlSignin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(validation()){

                        if(Utilites.isInternetOn(MyApplication.mContext)){
                            loginUser();
                        }else {
                            MyUtility.getInstance().showError(getContext(), mEdPassword, "Please check your Internet connection");
                        }

                    }
                }
            });


            mTvForgotPassword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent I = new Intent(getActivity(), ForgotPasswordActivity.class);
                    startActivity(I);
                }
            });


            mTvSignUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PushPopFragment.getInstance().pushFragment((AppCompatActivity) getActivity(), RegisterFragment.newInstance());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCompositeDisposable.dispose();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inf) {
      //  inf = getActivity().getMenuInflater();
//        inf.inflate(R.menu.all_charity_menu, menu);
        super.onCreateOptionsMenu(menu, inf);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {




        return true;
    }


    private boolean validation(){

        if(Utilites.validationInput(mEdEmail)){

            MyUtility.getInstance().showError(getContext(), mEdEmail, "Please enter email");
            return false;
        }


        if(!Utilites.isValidEmailEnter(mEdEmail.getText().toString().trim())){
            MyUtility.getInstance().showError(getContext(), mEdEmail, "Please enter valid email");
            return false;
        }

        if(Utilites.validationInput(mEdPassword)){

            MyUtility.getInstance().showError(getContext(), mEdPassword, "Please enter password");
            return false;
        }


        return true;
    }


    private  void loginUser(){

        try {
            CallProgressWheel.showLoadingDialog(getActivity(), "Loading...");
            ApiInterface apiInterface = RetrofitClient.createService(ApiInterface.class);

            JsonObject object = new JsonObject();

            object.addProperty("email", mEdEmail.getText().toString().trim());
            object.addProperty("password", mEdPassword.getText().toString().trim());

            mCompositeDisposable.add(apiInterface.login(object)

                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleResponse,this::handleError));
        }catch (Exception e){
            e.printStackTrace();
            CallProgressWheel.dismissLoadingDialog();
        }

    }


    private void handleResponse(ResponsePojo responsePojo) {
        CallProgressWheel.dismissLoadingDialog();
        if(responsePojo != null){

            if(responsePojo.getStatus_code() == 200) {
                Utilites.showToastMessageShort(MyApplication.mContext, responsePojo.getMessage());
                PreferenceHandler.writeInteger(MyApplication.mContext,PreferenceHandler.PREF_KEY_USER_ID,responsePojo.getUserid());

                Utilites utilites = new Utilites();
                utilites.getProfileData(getActivity(), responsePojo.getUserid(),mCompositeDisposable);

            }else if(responsePojo.getError() != null){
                Utilites.showToastMessageShort(MyApplication.mContext, responsePojo.getError().getDescription());
            }

        }
    }

    private void handleError(Throwable error) {

        CallProgressWheel.dismissLoadingDialog();
        Utilites.showToastMessageShort(MyApplication.mContext, Utilites.getErrorMessage(error, MyApplication.mContext));
    }

}
