package com.brst.aomnia.ui.fragments;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brst.aomnia.R;
import com.brst.aomnia.pojo.DataPojo;
import com.brst.aomnia.ui.activities.HomeActivity;
import com.brst.aomnia.utilities.MyApplication;
import com.brst.aomnia.utilities.MyUtility;
import com.brst.aomnia.utilities.PushPopFragment;
import com.brst.aomnia.utilities.Utilites;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OverViewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OverViewFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    View view;

    private OnFragmentInteractionListener mListener;

    TextView mTvMileage1,mTvMileage2, mTvMileage3, mTvFuel1, mTvFuel2, mTvFuel3,
            mTvDisp1, mTvDisp2,mTvDisp3,mTvmaker1, mTvmaker2, mTvmaker3,
            mTvModel1, mTvModel2, mTvModel3;

    LinearLayout mLiColor1, mLiColor2, mLiColor3;

    static DataPojo mDataPojo;
    public OverViewFragment() {
        // Required empty public constructor
    }


    public static OverViewFragment newInstance(DataPojo aDataPojo) {
        OverViewFragment fragment = new OverViewFragment();
        Bundle args = new Bundle();
        mDataPojo = aDataPojo;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

//        if (view != null) {
//
//            ViewGroup parent = (ViewGroup) view.getParent();
//            if (parent != null) {
//                parent.removeView(view);
//            }
//        }
        if (view==null) {
            view = inflater.inflate(R.layout.fragment_overview, container, false);

            setUpViews();
            setListner();

        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onResume() {
        super.onResume();



    }

    private void setUpViews() {

        try {

            mTvMileage1 = view.findViewById(R.id.text_view_milege1);
            mTvMileage2 = view.findViewById(R.id.text_view_milege2);
            mTvMileage3 = view.findViewById(R.id.text_view_milege3);


            mTvFuel1 = view.findViewById(R.id.text_view_fuel1);
            mTvFuel2 = view.findViewById(R.id.text_view_fuel2);
            mTvFuel3 = view.findViewById(R.id.text_view_fuel3);


            mTvDisp1 = view.findViewById(R.id.text_view_disp1);
            mTvDisp2 = view.findViewById(R.id.text_view_disp2);
            mTvDisp3 = view.findViewById(R.id.text_view_disp3);

            mTvmaker1 = view.findViewById(R.id.text_view_makers1);
            mTvmaker2 = view.findViewById(R.id.text_view_makers2);
            mTvmaker3 = view.findViewById(R.id.text_view_makers3);

            mTvModel1 = view.findViewById(R.id.text_view_model1);
            mTvModel2 = view.findViewById(R.id.text_view_model2);
            mTvModel3 = view.findViewById(R.id.text_view_model3);

            mLiColor1 = view.findViewById(R.id.layout_color1);
            mLiColor2 = view.findViewById(R.id.layout_color2);
            mLiColor3 = view.findViewById(R.id.layout_color3);


            if(mDataPojo.getVpackages().size() == 2){

                mTvMileage1.setText(mDataPojo.getVpackages().get(0).getOverview().get(0).getMileage());
                mTvMileage2.setText(mDataPojo.getVpackages().get(1).getOverview().get(0).getMileage());

                mTvFuel1.setText(mDataPojo.getVpackages().get(0).getOverview().get(0).getFueltype());
                mTvFuel2.setText(mDataPojo.getVpackages().get(1).getOverview().get(0).getFueltype());

                mTvDisp1.setText(mDataPojo.getVpackages().get(0).getOverview().get(0).getDisplacement());
                mTvDisp2.setText(mDataPojo.getVpackages().get(1).getOverview().get(0).getDisplacement());

                mTvmaker1.setText(mDataPojo.getVpackages().get(0).getOverview().get(0).getVmake());
                mTvmaker2.setText(mDataPojo.getVpackages().get(1).getOverview().get(0).getVmake());

                mTvModel1.setText(mDataPojo.getVpackages().get(0).getOverview().get(0).getVmodel());
                mTvModel2.setText(mDataPojo.getVpackages().get(1).getOverview().get(0).getVmodel());

                 mLiColor1.removeAllViews();
                for (String color : mDataPojo.getVpackages().get(0).getVcolor()){

                    int colorCode = Color.parseColor(color);

                    ImageView image = new ImageView(getActivity());

//                image.setLayoutParams(new android.view.ViewGroup.LayoutParams(15,15));

                    image.setMaxHeight(15);
                    image.setMaxWidth(15);

                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(15,15);
                    lp.setMargins(4, 0, 0, 0);
                    image.setLayoutParams(lp);

                    image.setColorFilter(colorCode);
                    image.setImageResource(R.mipmap.ellipse_white);

                    mLiColor1.addView( image);

                }


                mLiColor2.removeAllViews();
                for (String color : mDataPojo.getVpackages().get(1).getVcolor()){

                    int colorCode = Color.parseColor(color);

                    ImageView image = new ImageView(getActivity());

//                image.setLayoutParams(new android.view.ViewGroup.LayoutParams(15,15));

                    image.setMaxHeight(15);
                    image.setMaxWidth(15);

                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(15,15);
                    lp.setMargins(4, 0, 0, 0);
                    image.setLayoutParams(lp);

                    image.setColorFilter(colorCode);
                    image.setImageResource(R.mipmap.ellipse_white);

                    mLiColor2.addView( image);

                }



            }


            if (mDataPojo.getVpackages().size() == 3){
                mTvMileage1.setText(mDataPojo.getVpackages().get(0).getOverview().get(0).getMileage());
                mTvMileage2.setText(mDataPojo.getVpackages().get(1).getOverview().get(0).getMileage());
                mTvMileage3.setText(mDataPojo.getVpackages().get(2).getOverview().get(0).getMileage());

                mTvFuel1.setText(mDataPojo.getVpackages().get(0).getOverview().get(0).getFueltype());
                mTvFuel2.setText(mDataPojo.getVpackages().get(1).getOverview().get(0).getFueltype());
                mTvFuel3.setText(mDataPojo.getVpackages().get(2).getOverview().get(0).getFueltype());

                mTvDisp1.setText(mDataPojo.getVpackages().get(0).getOverview().get(0).getDisplacement());
                mTvDisp2.setText(mDataPojo.getVpackages().get(1).getOverview().get(0).getDisplacement());
                mTvDisp3.setText(mDataPojo.getVpackages().get(2).getOverview().get(0).getDisplacement());

                mTvmaker1.setText(mDataPojo.getVpackages().get(0).getOverview().get(0).getVmake());
                mTvmaker2.setText(mDataPojo.getVpackages().get(1).getOverview().get(0).getVmake());
                mTvmaker3.setText(mDataPojo.getVpackages().get(2).getOverview().get(0).getVmake());


                mTvModel1.setText(mDataPojo.getVpackages().get(0).getOverview().get(0).getVmodel());
                mTvModel2.setText(mDataPojo.getVpackages().get(1).getOverview().get(0).getVmodel());
                mTvModel3.setText(mDataPojo.getVpackages().get(2).getOverview().get(0).getVmodel());


                mLiColor1.removeAllViews();
                for (String color : mDataPojo.getVpackages().get(0).getVcolor()){

                    int colorCode = Color.parseColor(color);

                    ImageView image = new ImageView(getActivity());

//                image.setLayoutParams(new android.view.ViewGroup.LayoutParams(15,15));

                    image.setMaxHeight(15);
                    image.setMaxWidth(15);

                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(15,15);
                    lp.setMargins(4, 0, 0, 0);
                    image.setLayoutParams(lp);

                    image.setColorFilter(colorCode);
                    image.setImageResource(R.mipmap.ellipse_white);

                    mLiColor1.addView( image);

                }


                mLiColor2.removeAllViews();
                for (String color : mDataPojo.getVpackages().get(1).getVcolor()){

                    int colorCode = Color.parseColor(color);

                    ImageView image = new ImageView(getActivity());

//                image.setLayoutParams(new android.view.ViewGroup.LayoutParams(15,15));

                    image.setMaxHeight(15);
                    image.setMaxWidth(15);

                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(15,15);
                    lp.setMargins(4, 0, 0, 0);
                    image.setLayoutParams(lp);

                    image.setColorFilter(colorCode);
                    image.setImageResource(R.mipmap.ellipse_white);

                    mLiColor2.addView( image);

                }


                mLiColor3.removeAllViews();
                for (String color : mDataPojo.getVpackages().get(2).getVcolor()){

                    int colorCode = Color.parseColor(color);

                    ImageView image = new ImageView(getActivity());

//                image.setLayoutParams(new android.view.ViewGroup.LayoutParams(15,15));

                    image.setMaxHeight(15);
                    image.setMaxWidth(15);

                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(15,15);
                    lp.setMargins(4, 0, 0, 0);
                    image.setLayoutParams(lp);

                    image.setColorFilter(colorCode);
                    image.setImageResource(R.mipmap.ellipse_white);

                    mLiColor3.addView( image);

                }



            }


        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private void setListner() {

        try {


        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inf) {
        //  inf = getActivity().getMenuInflater();
//        inf.inflate(R.menu.all_charity_menu, menu);
        super.onCreateOptionsMenu(menu, inf);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {




        return true;
    }




}
