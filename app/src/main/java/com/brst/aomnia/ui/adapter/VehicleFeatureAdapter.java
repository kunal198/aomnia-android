package com.brst.aomnia.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.brst.aomnia.R;
import com.brst.aomnia.pojo.DataPojo;
import com.brst.aomnia.ui.fragments.FeatureSingleVehicleFragment;
import com.brst.aomnia.ui.fragments.FeaturesFragment;
import com.brst.aomnia.ui.fragments.OverViewFragment;
import com.brst.aomnia.ui.fragments.OverViewSingleVehicleFragment;
import com.brst.aomnia.ui.fragments.SpecificationSingleVehicleFragment;


/**
 * Created by brst-pc89 on 12/6/17.
 */


public class VehicleFeatureAdapter extends FragmentStatePagerAdapter {

    private Context context;

    int size;
    DataPojo mPojo;

    public VehicleFeatureAdapter(Context context, FragmentManager fm, int size, DataPojo aPojo) {

        super(fm);
        this.context = context;
        this.size = size;
        this.mPojo = aPojo;

    }


//    @Override
//    public Fragment getItem(int position) {
//
//
//    }int

    // This determines the number of tabs
    @Override
    public int getCount() {

        return size;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position

        switch (position) {

            case 0:
                return context.getString(R.string.overview);
            case 1:
                return context.getString(R.string.features);

            case 2:
                return context.getString(R.string.specs);

            default:
                return null;
        }
    }


    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch(position){
            case 0:
                fragment = OverViewSingleVehicleFragment.newInstance(mPojo);
                break;
            case 1:
                fragment = FeatureSingleVehicleFragment.newInstance(mPojo);
                break;
            case 2:
                fragment = SpecificationSingleVehicleFragment.newInstance(mPojo);
                break;

        }
        return fragment;
    }
}


