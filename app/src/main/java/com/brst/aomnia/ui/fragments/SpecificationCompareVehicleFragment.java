package com.brst.aomnia.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brst.aomnia.R;
import com.brst.aomnia.pojo.DataPojo;
import com.brst.aomnia.ui.adapter.VehicleOverviewSingleVehicleAdapter;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SpecificationCompareVehicleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SpecificationCompareVehicleFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    View view;

    private OnFragmentInteractionListener mListener;
    RecyclerView mRvOverview;
    VehicleOverviewSingleVehicleAdapter adapter;

    List<String> listOverView;

    static DataPojo pojo;

    TextView mTvLength1, mTvWidth1, mTvWheelbase1, mTvLength2, mTvWidth2, mTvWheelbase2, mTvLength3, mTvWidth3, mTvWheelbase3;
    TextView   mTvseatingcap1, mTvbootspace1, mTvFueltank1,mTvseatingcap2, mTvbootspace2, mTvFueltank2,mTvseatingcap3, mTvbootspace3, mTvFueltank3;

    TextView mTvDisplacement1 , mTvEngineType1,  mTvalve1, mTvMileage, mTvFuelType, mTvCylinder,
            mTvDisplacement2 , mTvEngineType2,  mTvalve2, mTvMileage2, mTvFuelType2, mTvCylinder2
            ,mTvDisplacement3 , mTvEngineType3,  mTvalve3, mTvMileage3, mTvFuelType3, mTvCylinder3;

    public SpecificationCompareVehicleFragment() {
        // Required empty public constructor
    }



    public static SpecificationCompareVehicleFragment newInstance(DataPojo aPojo) {
        SpecificationCompareVehicleFragment fragment = new SpecificationCompareVehicleFragment();
        pojo = aPojo;
        Bundle args = new Bundle();


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

//        if (view != null) {
//
//            ViewGroup parent = (ViewGroup) view.getParent();
//            if (parent != null) {
//                parent.removeView(view);
//            }
//        }
        if (view==null) {
            view = inflater.inflate(R.layout.fragment_specs_compare_vehicle, container, false);

            setUpViews();
            setListner();

        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onResume() {
        super.onResume();



    }

    private void setUpViews() {

        try {

            // 1st row
            mTvLength1 = view.findViewById(R.id.text_view_length1);
            mTvLength2 = view.findViewById(R.id.text_view_length2);
            mTvLength3 = view.findViewById(R.id.text_view_length3);

            mTvWidth1 = view.findViewById(R.id.text_view_width1);
            mTvWidth2 = view.findViewById(R.id.text_view_width2);
            mTvWidth3 = view.findViewById(R.id.text_view_width3);

            mTvWheelbase1 = view.findViewById(R.id.text_view_wheelbase1);
            mTvWheelbase2 = view.findViewById(R.id.text_view_wheelbase2);
            mTvWheelbase3 = view.findViewById(R.id.text_view_wheelbase3);
            // end row


            // 2nd row
            mTvseatingcap1 = view.findViewById(R.id.text_view_seal_cap1);
            mTvseatingcap2 = view.findViewById(R.id.text_view_seal_cap2);
            mTvseatingcap3 = view.findViewById(R.id.text_view_seal_cap3);

            mTvbootspace1 = view.findViewById(R.id.text_view_boat_sp1);
            mTvbootspace2 = view.findViewById(R.id.text_view_boat_sp2);
            mTvbootspace3 = view.findViewById(R.id.text_view_boat_sp3);

            mTvFueltank1 = view.findViewById(R.id.text_view_fuel_sp1);
            mTvFueltank2 = view.findViewById(R.id.text_view_fuel_sp1);
            mTvFueltank3 = view.findViewById(R.id.text_view_fuel_sp3);
            // end row


            // 3rd row
            mTvEngineType1 = view.findViewById(R.id.text_view_engine1);
            mTvEngineType2 = view.findViewById(R.id.text_view_engine2);
            mTvEngineType3 = view.findViewById(R.id.text_view_engine3);

            mTvalve1 = view.findViewById(R.id.text_view_valve1);
            mTvalve2 = view.findViewById(R.id.text_view_valve2);
            mTvalve3 = view.findViewById(R.id.text_view_valve3);

            mTvDisplacement1 = view.findViewById(R.id.text_view_disp1);
            mTvDisplacement2 = view.findViewById(R.id.text_view_disp2);
            mTvDisplacement3 = view.findViewById(R.id.text_view_disp3);
            // end row



            if(pojo.getVpackages().size() == 2){

                mTvLength1.setText(pojo.getVpackages().get(0).getSpecificationobj().getDimensions().getLength().getName()+": "+pojo.getVpackages().get(0).getSpecificationobj().getDimensions().getLength().getValue());
                mTvLength2.setText(pojo.getVpackages().get(1).getSpecificationobj().getDimensions().getLength().getName()+": "+pojo.getVpackages().get(1).getSpecificationobj().getDimensions().getLength().getValue());

                mTvWidth1.setText(pojo.getVpackages().get(0).getSpecificationobj().getDimensions().getWidth().getName()+": "+pojo.getVpackages().get(0).getSpecificationobj().getDimensions().getWidth().getValue());
                mTvWidth2.setText(pojo.getVpackages().get(1).getSpecificationobj().getDimensions().getWidth().getName()+": "+pojo.getVpackages().get(1).getSpecificationobj().getDimensions().getWidth().getValue());

                mTvWheelbase1.setText(pojo.getVpackages().get(0).getSpecificationobj().getDimensions().getWheelbase().getName()+": "+pojo.getVpackages().get(0).getSpecificationobj().getDimensions().getWheelbase().getValue());
                mTvWheelbase2.setText(pojo.getVpackages().get(1).getSpecificationobj().getDimensions().getWheelbase().getName()+": "+pojo.getVpackages().get(1).getSpecificationobj().getDimensions().getWheelbase().getValue());


                mTvseatingcap1.setText(pojo.getVpackages().get(0).getSpecificationobj().getCapacity().getSeatingCapacity().getName()+": "+pojo.getVpackages().get(0).getSpecificationobj().getCapacity().getSeatingCapacity().getValue());
                mTvseatingcap2.setText(pojo.getVpackages().get(1).getSpecificationobj().getCapacity().getSeatingCapacity().getName()+": "+pojo.getVpackages().get(1).getSpecificationobj().getCapacity().getSeatingCapacity().getValue());


                mTvbootspace1.setText(pojo.getVpackages().get(0).getSpecificationobj().getCapacity().getBookspace().getName()+": "+pojo.getVpackages().get(0).getSpecificationobj().getDimensions().getWheelbase().getValue());
                mTvbootspace2.setText(pojo.getVpackages().get(1).getSpecificationobj().getCapacity().getBookspace().getName()+": "+pojo.getVpackages().get(1).getSpecificationobj().getCapacity().getBookspace().getValue());

                mTvFueltank1.setText(pojo.getVpackages().get(0).getSpecificationobj().getCapacity().getFueltank().getName()+": "+pojo.getVpackages().get(0).getSpecificationobj().getCapacity().getFueltank().getValue());
                mTvFueltank2.setText(pojo.getVpackages().get(1).getSpecificationobj().getCapacity().getFueltank().getName()+": "+pojo.getVpackages().get(1).getSpecificationobj().getCapacity().getFueltank().getValue());

                mTvEngineType1.setText(pojo.getVpackages().get(0).getSpecificationobj().getEngine().getEngineType().getName()+": "+pojo.getVpackages().get(0).getSpecificationobj().getEngine().getEngineType().getValue());
                mTvEngineType2.setText(pojo.getVpackages().get(1).getSpecificationobj().getEngine().getEngineType().getName()+": "+pojo.getVpackages().get(1).getSpecificationobj().getEngine().getEngineType().getValue());

                mTvalve1.setText(pojo.getVpackages().get(0).getSpecificationobj().getEngine().getValve().getName()+": "+pojo.getVpackages().get(0).getSpecificationobj().getEngine().getValve().getValue());
                mTvalve2.setText(pojo.getVpackages().get(1).getSpecificationobj().getEngine().getValve().getName()+": "+pojo.getVpackages().get(1).getSpecificationobj().getEngine().getValve().getValue());

                mTvDisplacement1.setText(pojo.getVpackages().get(0).getSpecificationobj().getEngine().getDisplacement().getName()+": "+pojo.getVpackages().get(0).getSpecificationobj().getEngine().getDisplacement().getValue());
                mTvDisplacement2.setText(pojo.getVpackages().get(1).getSpecificationobj().getEngine().getDisplacement().getName()+": "+pojo.getVpackages().get(1).getSpecificationobj().getEngine().getDisplacement().getValue());


            }


            if(pojo.getVpackages().size() == 3){

                mTvLength1.setText(pojo.getVpackages().get(0).getSpecificationobj().getDimensions().getLength().getName()+": "+pojo.getVpackages().get(0).getSpecificationobj().getDimensions().getLength().getValue());
                mTvLength2.setText(pojo.getVpackages().get(1).getSpecificationobj().getDimensions().getLength().getName()+": "+pojo.getVpackages().get(1).getSpecificationobj().getDimensions().getLength().getValue());
                mTvLength3.setText(pojo.getVpackages().get(2).getSpecificationobj().getDimensions().getLength().getName()+": "+pojo.getVpackages().get(2).getSpecificationobj().getDimensions().getLength().getValue());

                mTvWidth1.setText(pojo.getVpackages().get(0).getSpecificationobj().getDimensions().getWidth().getName()+": "+pojo.getVpackages().get(0).getSpecificationobj().getDimensions().getWidth().getValue());
                mTvWidth2.setText(pojo.getVpackages().get(1).getSpecificationobj().getDimensions().getWidth().getName()+": "+pojo.getVpackages().get(1).getSpecificationobj().getDimensions().getWidth().getValue());
                mTvWidth3.setText(pojo.getVpackages().get(2).getSpecificationobj().getDimensions().getWidth().getName()+": "+pojo.getVpackages().get(2).getSpecificationobj().getDimensions().getWidth().getValue());

                mTvWheelbase1.setText(pojo.getVpackages().get(0).getSpecificationobj().getDimensions().getWheelbase().getName()+": "+pojo.getVpackages().get(0).getSpecificationobj().getDimensions().getWheelbase().getValue());
                mTvWheelbase2.setText(pojo.getVpackages().get(1).getSpecificationobj().getDimensions().getWheelbase().getName()+": "+pojo.getVpackages().get(1).getSpecificationobj().getDimensions().getWheelbase().getValue());
                mTvWheelbase3.setText(pojo.getVpackages().get(2).getSpecificationobj().getDimensions().getWheelbase().getName()+": "+pojo.getVpackages().get(2).getSpecificationobj().getDimensions().getWheelbase().getValue());



                mTvseatingcap1.setText(pojo.getVpackages().get(0).getSpecificationobj().getCapacity().getSeatingCapacity().getName()+": "+pojo.getVpackages().get(0).getSpecificationobj().getCapacity().getSeatingCapacity().getValue());
                mTvseatingcap2.setText(pojo.getVpackages().get(1).getSpecificationobj().getCapacity().getSeatingCapacity().getName()+": "+pojo.getVpackages().get(1).getSpecificationobj().getCapacity().getSeatingCapacity().getValue());
                mTvseatingcap3.setText(pojo.getVpackages().get(2).getSpecificationobj().getCapacity().getSeatingCapacity().getName()+": "+pojo.getVpackages().get(2).getSpecificationobj().getCapacity().getSeatingCapacity().getValue());


                mTvbootspace1.setText(pojo.getVpackages().get(0).getSpecificationobj().getCapacity().getBookspace().getName()+": "+pojo.getVpackages().get(0).getSpecificationobj().getDimensions().getWheelbase().getValue());
                mTvbootspace2.setText(pojo.getVpackages().get(1).getSpecificationobj().getCapacity().getBookspace().getName()+": "+pojo.getVpackages().get(1).getSpecificationobj().getCapacity().getBookspace().getValue());
                mTvbootspace3.setText(pojo.getVpackages().get(2).getSpecificationobj().getCapacity().getBookspace().getName()+": "+pojo.getVpackages().get(2).getSpecificationobj().getCapacity().getBookspace().getValue());

                mTvFueltank1.setText(pojo.getVpackages().get(0).getSpecificationobj().getCapacity().getFueltank().getName()+": "+pojo.getVpackages().get(0).getSpecificationobj().getCapacity().getFueltank().getValue());
                mTvFueltank2.setText(pojo.getVpackages().get(1).getSpecificationobj().getCapacity().getFueltank().getName()+": "+pojo.getVpackages().get(1).getSpecificationobj().getCapacity().getFueltank().getValue());
                mTvFueltank3.setText(pojo.getVpackages().get(2).getSpecificationobj().getCapacity().getFueltank().getName()+": "+pojo.getVpackages().get(2).getSpecificationobj().getCapacity().getFueltank().getValue());

                mTvEngineType1.setText(pojo.getVpackages().get(0).getSpecificationobj().getEngine().getEngineType().getName()+": "+pojo.getVpackages().get(0).getSpecificationobj().getEngine().getEngineType().getValue());
                mTvEngineType2.setText(pojo.getVpackages().get(1).getSpecificationobj().getEngine().getEngineType().getName()+": "+pojo.getVpackages().get(1).getSpecificationobj().getEngine().getEngineType().getValue());
                mTvEngineType3.setText(pojo.getVpackages().get(2).getSpecificationobj().getEngine().getEngineType().getName()+": "+pojo.getVpackages().get(2).getSpecificationobj().getEngine().getEngineType().getValue());

                mTvalve1.setText(pojo.getVpackages().get(0).getSpecificationobj().getEngine().getValve().getName()+": "+pojo.getVpackages().get(0).getSpecificationobj().getEngine().getValve().getValue());
                mTvalve2.setText(pojo.getVpackages().get(1).getSpecificationobj().getEngine().getValve().getName()+": "+pojo.getVpackages().get(1).getSpecificationobj().getEngine().getValve().getValue());
                mTvalve3.setText(pojo.getVpackages().get(2).getSpecificationobj().getEngine().getValve().getName()+": "+pojo.getVpackages().get(2).getSpecificationobj().getEngine().getValve().getValue());

                mTvDisplacement1.setText(pojo.getVpackages().get(0).getSpecificationobj().getEngine().getDisplacement().getName()+": "+pojo.getVpackages().get(0).getSpecificationobj().getEngine().getDisplacement().getValue());
                mTvDisplacement2.setText(pojo.getVpackages().get(1).getSpecificationobj().getEngine().getDisplacement().getName()+": "+pojo.getVpackages().get(1).getSpecificationobj().getEngine().getDisplacement().getValue());
                mTvDisplacement3.setText(pojo.getVpackages().get(2).getSpecificationobj().getEngine().getDisplacement().getName()+": "+pojo.getVpackages().get(2).getSpecificationobj().getEngine().getDisplacement().getValue());



            }


        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private void setListner() {

        try {

            setAdapter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inf) {
        //  inf = getActivity().getMenuInflater();
//        inf.inflate(R.menu.all_charity_menu, menu);
        super.onCreateOptionsMenu(menu, inf);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {




        return true;
    }


    private void setAdapter(){


    }

}
