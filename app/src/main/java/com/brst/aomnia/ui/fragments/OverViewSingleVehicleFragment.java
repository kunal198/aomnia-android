package com.brst.aomnia.ui.fragments;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brst.aomnia.R;
import com.brst.aomnia.pojo.DataPojo;
import com.brst.aomnia.pojo.Overview;
import com.brst.aomnia.ui.adapter.FeaturesAdapter;
import com.brst.aomnia.ui.adapter.VehicleOverviewSingleVehicleAdapter;
import com.brst.aomnia.utilities.SpacesItemDecoration;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OverViewSingleVehicleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OverViewSingleVehicleFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    View view;
    TextView  mTvmaker, mTvDisplacement, mTvMilege, mTvCyclinder;
    private OnFragmentInteractionListener mListener;
    RecyclerView mRvOverview;
    VehicleOverviewSingleVehicleAdapter adapter;

    List<String> listOverView;
    LinearLayout mLayoutColors;
    static DataPojo pojo;
    public OverViewSingleVehicleFragment() {
        // Required empty public constructor
    }



    public static OverViewSingleVehicleFragment newInstance(DataPojo aPojo) {
        OverViewSingleVehicleFragment fragment = new OverViewSingleVehicleFragment();
        pojo = aPojo;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

//        if (view != null) {
//
//            ViewGroup parent = (ViewGroup) view.getParent();
//            if (parent != null) {
//                parent.removeView(view);
//            }
//        }
        if (view==null) {
            view = inflater.inflate(R.layout.fragment_overview_single_vehicle, container, false);

            setUpViews();
            setListner();

        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onResume() {
        super.onResume();



    }

    private void setUpViews() {

        try {

//            mRvOverview =  view.findViewById(R.id.rv_overview);


            mTvmaker = view.findViewById(R.id.text_view_makers);
            mTvDisplacement = view.findViewById(R.id.text_view_displacement);
            mTvMilege = view.findViewById(R.id.text_view_milege);
            mTvCyclinder = view.findViewById(R.id.text_view_cylinder);
            mLayoutColors =view.findViewById(R.id.layout_colors);


            mTvmaker.setText("Maker: "+pojo.getOverview().get(0).getVmake()+",  Model: "+pojo.getOverview().get(0).getVmodel()+"");

            mTvDisplacement.setText(pojo.getOverview().get(0).getDisplacement());
            mTvMilege.setText(pojo.getOverview().get(0).getMileage());
            mTvCyclinder.setText("FuelType"+pojo.getOverview().get(0).getFueltype()+",  Cylinder: "+pojo.getOverview().get(0).getCylinder()+"");


            mLayoutColors.removeAllViews();
            for (String color : pojo.getOverview().get(0).getVcolor()){

                int colorCode = Color.parseColor(color);

                ImageView image = new ImageView(getActivity());

//                image.setLayoutParams(new android.view.ViewGroup.LayoutParams(15,15));

                image.setMaxHeight(15);
                image.setMaxWidth(15);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(15,15);
                lp.setMargins(4, 0, 0, 0);
                image.setLayoutParams(lp);

                image.setColorFilter(colorCode);
                image.setImageResource(R.mipmap.ellipse_white);

                mLayoutColors.addView( image);

            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private void setListner() {

        try {

//            setAdapter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inf) {
        //  inf = getActivity().getMenuInflater();
//        inf.inflate(R.menu.all_charity_menu, menu);
        super.onCreateOptionsMenu(menu, inf);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {




        return true;
    }





}
