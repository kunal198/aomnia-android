package com.brst.aomnia.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.aomnia.R;
import com.brst.aomnia.controller.ApiInterface;
import com.brst.aomnia.controller.RetrofitClient;
import com.brst.aomnia.pojo.ResponsePojo;
import com.brst.aomnia.ui.baseactivities.BaseActivity;
import com.brst.aomnia.ui.fragments.AboutUsFragment;
import com.brst.aomnia.ui.fragments.FAQFragment;
import com.brst.aomnia.ui.fragments.HowItWorkFragment;
import com.brst.aomnia.ui.fragments.LoginFragment;
import com.brst.aomnia.ui.fragments.SearchVehiclesFragment;
import com.brst.aomnia.utilities.CallProgressWheel;
import com.brst.aomnia.utilities.MyApplication;
import com.brst.aomnia.utilities.MyUtility;
import com.brst.aomnia.utilities.PreferenceHandler;
import com.brst.aomnia.utilities.Utilites;
import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by brst-pc97 on 1/22/18.
 */

public class HomeActivity  extends BaseActivity {


    public static NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private ImageView imgNavHeaderBg;
    public static TextView txtName, text_toolbar_title, txtEmail;
    public static Toolbar toolbar;
    RelativeLayout view_container;
    private CompositeDisposable mCompositeDisposable;

    // urls to load navigation header background image
    // and profile image

    private static final String urlProfileImg = "";

    // index to identify current nav menu item
    public static int navItemIndex = 0;

    // tags used to attach the fragments
    private static final String TAG_LOGIN_SIGNUP = "login_signup";
    private static final String TAG_SEARCH = "search vehicles";
    private static final String TAG_HOW_IT_WORK = "charity";
    private static final String TAG_LOGIN = "category";
    private static final String TAG_SIGNUP= "favourite";

    private static final String TAG_ABOUT_US = "history";
    private static final String TAG_FAQ = "profile";

    public static String CURRENT_TAG = TAG_SEARCH;

    // toolbar titles respected to selected nav menu item
    public static String[] activityTitles;

    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;
    Context context;

    public static FrameLayout frameLayoutDonate;

    public static AppBarLayout htab_appbar;
    int userId = -1;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCompositeDisposable = new CompositeDisposable();
        userId = PreferenceHandler.readInteger(MyApplication.mContext,PreferenceHandler.PREF_KEY_USER_ID,-1);



        setUpViews();

        setUpData();

        loadNavHeader();

        // initializing navigation menu
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_SEARCH;
            loadHomeFragment();
        }

        if(userId != -1){

            txtName.setText(PreferenceHandler.readString(MyApplication.mContext,PreferenceHandler.PREF_KEY_FIRST_NAME,""));
            txtEmail.setText(PreferenceHandler.readString(MyApplication.mContext,PreferenceHandler.PREF_KEY_EMAIL,""));
            navigationView.getMenu().getItem(2).setVisible(false);
            navigationView.getMenu().getItem(3).setVisible(true);
            navigationView.getMenu().getItem(6).setVisible(true);

        }else {
            navigationView.getMenu().getItem(7).setVisible(false);
            navigationView.getMenu().getItem(3).setVisible(false);
            navigationView.getMenu().getItem(6).setVisible(false);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCompositeDisposable.dispose();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(userId != -1) {

            txtName.setText(PreferenceHandler.readString(MyApplication.mContext, PreferenceHandler.PREF_KEY_FIRST_NAME, ""));
            txtEmail.setText(PreferenceHandler.readString(MyApplication.mContext, PreferenceHandler.PREF_KEY_EMAIL, ""));
        }
    }

    @Override
    public void setUpViews() {

        context = this;

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        frameLayoutDonate = findViewById(R.id.frameLayoutDonate);
        //  frameLayoutDonate = findViewById(R.id.frameLayoutDonate);

        setSupportActionBar(toolbar);

        mHandler = new Handler();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);


        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        txtName = navHeader.findViewById(R.id.name);
        txtEmail = navHeader.findViewById(R.id.email);

        imgNavHeaderBg = navHeader.findViewById(R.id.img_header_bg);
//        imgProfile = navHeader.findViewById(R.id.img_profile);
        text_toolbar_title = findViewById(R.id.text_toolbar_title);
        text_toolbar_title.setVisibility(View.GONE);
        view_container = navHeader.findViewById(R.id.view_container);
        htab_appbar = findViewById(R.id.htab_appbar);
        // load toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);


        // frameLayoutDonate.setOnClickListener();


        Utilites.callBackSearch(new Utilites.callBackToSearch() {
            @Override
            public void onClickSearch() {
                navItemIndex = 0;
                CURRENT_TAG = TAG_SEARCH;
                loadHomeFragment();


            }
        });


    }

    @Override
    public void setUpData() {

        // load nav menu header data


    }

    @Override
    public void onClick(View v) {

    }


    /***
     * Load navigation menu header information
     * like background image, profile image
     * name, website, notifications action view (dot)
     */
    private void loadNavHeader() {
        // name, website
        txtName.setText("Omnia");


        // loading header background image


        // Loading profile image

      /*  Glide.with(this).load(urlProfileImg)
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgProfile);*/


        view_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });

        // showing dot next to notifications label
        //  navigationView.getMenu().getItem(3).setActionView(R.layout.menu_dot);
    }

    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadHomeFragment() {

        // frameLayoutDonate.setVisibility(View.GONE);
        // selecting appropriate nav menu item
        selectNavMenu();

        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
//        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
//            drawer.closeDrawers();
//
//            // show or hide the fab button
////            toggleFab();
//            return;
//        }





        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments

                //   try {


                try {
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }catch (Exception e){

                }



                Fragment fragment = getHomeFragment();

                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                //     fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                // PREVIOUS_TAG = CURRENT_TAG;
               /* }
                catch (Exception e)
                {

                }*/
            }
        };




        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.postDelayed(mPendingRunnable,200);
        }

        // show or hide the fab button
//        toggleFab();

        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    Fragment fragment;

    private Fragment getHomeFragment() {

        switch (navItemIndex) {
            case 0:
                // home
                SearchVehiclesFragment searchVehiclesFragment = SearchVehiclesFragment.newInstance();

                fragment=searchVehiclesFragment;

                return searchVehiclesFragment;

            case 1:

                HowItWorkFragment howItWorkFragment = HowItWorkFragment.newInstance();

                fragment=howItWorkFragment;

                return fragment;



            case 2:
                LoginFragment loginFragment = LoginFragment.newInstance();

                fragment=loginFragment;

                return fragment;






            case 4:
                AboutUsFragment aboutUsFragment = AboutUsFragment.newInstance();

                fragment=aboutUsFragment;

                return fragment;

            case 5:
                FAQFragment faqFragment =FAQFragment.newInstance();

                fragment=faqFragment;

                return fragment;



            default:

                fragment=SearchVehiclesFragment.newInstance();
                return fragment;

        }



    }

    public void setToolbarTitle() {

        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {

        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {


        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                MyUtility.getInstance().hideKeypad(context);


                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_search_vechile:

                        navItemIndex = 0;

                        CURRENT_TAG = TAG_SEARCH;
                        // loadHomeFr CURRENT_TAG = TAG_HOME;agment();
                        //  startActivity(new Intent(CharityHomeActivity.this, ForgotActivity.class));
                        // PushPopFragment.getInstance().pushFragment(CharityHomeActivity.this,new SmartDonateFragment());

                        break;

                    case R.id.nav_how_it_work:

                        navItemIndex = 1;
                        CURRENT_TAG = TAG_HOW_IT_WORK;

                        Toast.makeText(context, "InProgress...", Toast.LENGTH_SHORT).show();

                        break;

                    case R.id.nav_login_sign_up:

                        userId = PreferenceHandler.readInteger(MyApplication.mContext,PreferenceHandler.PREF_KEY_USER_ID,-1);
                        if(userId == -1) {
                            navItemIndex = 2;
                            CURRENT_TAG = TAG_LOGIN_SIGNUP;

                        }else {

                            Utilites.showToastMessageShort(HomeActivity.this,"User already Logged in");
                            drawer.closeDrawers();

                            // refresh toolbar menu
                            invalidateOptionsMenu();
                            return true;
                        }
                        break;


                    case R.id.nav_myaccount:

                        navItemIndex = 3;

                        Intent I = new Intent(HomeActivity.this, RequestResultActivity.class);
                         startActivity(I);

//                        Toast.makeText(context, "InProgress...", Toast.LENGTH_SHORT).show();
                        drawer.closeDrawers();

                        // refresh toolbar menu
                        invalidateOptionsMenu();
                        return true;

                    case R.id.nav_about_us:

                        navItemIndex = 4;
                        CURRENT_TAG = TAG_ABOUT_US;

                        Toast.makeText(context, "InProgress...", Toast.LENGTH_SHORT).show();

                        break;

                    case R.id.nav_faq:

                        navItemIndex = 5;
                        CURRENT_TAG = TAG_FAQ;
                        break;


                    case R.id.nav_change_password:

                        navItemIndex = 6;

                        Intent I1 = new Intent(HomeActivity.this, ChangePasswordActivity.class);
                        startActivity(I1);

//                        Toast.makeText(context, "InProgress...", Toast.LENGTH_SHORT).show();
                        drawer.closeDrawers();

                        // refresh toolbar menu
                        invalidateOptionsMenu();
                        return true;

                    case R.id.nav_logout:


                        try {

                            AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                            builder.setTitle("Logout");
                            builder.setMessage("Are you sure you want to logout?");

                            builder.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // positive button logic
                                           dialog.dismiss();
                                            navItemIndex = 2;
                                            CURRENT_TAG = TAG_LOGIN_SIGNUP;

                                            if (menuItem.isChecked()) {
                                                menuItem.setChecked(false);
                                            } else {
                                                menuItem.setChecked(true);
                                            }
                                            menuItem.setChecked(true);
                                            PreferenceHandler.writeInteger(MyApplication.mContext,PreferenceHandler.PREF_KEY_USER_ID, -1);
                                            PreferenceHandler.writeInteger(MyApplication.mContext,PreferenceHandler.PREF_KEY_VEHICLE_ID, -1);
                                            PreferenceHandler.writeBoolean(MyApplication.mContext,PreferenceHandler.PREF_KEY_ID_REQUEST_USER_DEMO,false);

                                            HomeActivity.txtEmail.setText("info@omnia@gmail.com");
                                            HomeActivity.txtName.setText("omnia");

                                            HomeActivity.navigationView.getMenu().getItem(2).setVisible(true);
                                            HomeActivity.navigationView.getMenu().getItem(3).setVisible(false);
                                            HomeActivity.navigationView.getMenu().getItem(6).setVisible(false);
                                            HomeActivity.navigationView.getMenu().getItem(7).setVisible(false);

                                            loadHomeFragment();
                                        }
                                    });


                            builder.setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();

                                            // negative button logic
                                        }
                                    });
                            builder.setCancelable(false);
                            AlertDialog dialog = builder.create();
                            // display dialog
                            dialog.show();
                        } catch (Exception e) {
                            e.getStackTrace();
                        }

//                        Utilites.showLogoutPopUp(HomeActivity.this);
                        drawer.closeDrawers();

                        // refresh toolbar menu
                        invalidateOptionsMenu();



                        return true;



                    default:
                        navItemIndex = 0;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
       /* if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home
            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;
                loadHomeFragment();
                return;
            }
        }*/

        try {

            int entry_count = getSupportFragmentManager().getBackStackEntryCount();

            if (entry_count == 0) {

             /*   if (navItemIndex != 0) {
                    navItemIndex = 0;
                    CURRENT_TAG = TAG_HOME;
                    loadHomeFragment();
                    return;
                }*/
                // finish();
                MyUtility.getInstance().showAlertDialog(HomeActivity.this, getResources().getString(R.string.alertexit_name), true);

                //super.onBackPressed();
            } else {

                super.onBackPressed();
//                PushPopFragment.getInstance().popBackStack(this);
                //  PREVIOUS_TAG = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // frameLayoutDonate.setVisibility(View.GONE);
        //   super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.


      /*  if (navItemIndex == 4) {
            getMenuInflater().inflate(R.menu.menu_history, menu);
        }*/

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.actionScan) {
//            //  Toast.makeText(getApplicationContext(), "Logout user!", Toast.LENGTH_LONG).show();
//
//            startActivity(new Intent(CharityHomeActivity.this, SimpleScannerActivity.class));
//
//
//            return true;
//        }
//
//        if (id == R.id.actionClean) {
//            Toast.makeText(getApplicationContext(), "InProgess...", Toast.LENGTH_LONG).show();
//            return true;
//        }
//        if (id == R.id.actionLogout) {
//            MyUtility.getInstance().showAlertDialog(context, getResources().getString(R.string.alertlogout_name), false);
//        }


        return super.onOptionsItemSelected(item);
    }


//    private void getProfileData(int userId){
//
//        try {
//            CallProgressWheel.showLoadingDialog(this, "Loading...");
//            ApiInterface apiInterface = RetrofitClient.createService(ApiInterface.class);
//
//            JsonObject object = new JsonObject();
//            object.addProperty("userid", userId);
//
//
//            mCompositeDisposable.add(apiInterface.getProfile(object)
//
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribeOn(Schedulers.io())
//                    .subscribe(this::handleResponse,this::handleError));
//        }catch (Exception e){
//            e.printStackTrace();
//            CallProgressWheel.dismissLoadingDialog();
//        }
//
//
//    }
//
//    private void handleResponse(ResponsePojo responsePojo) {
//        CallProgressWheel.dismissLoadingDialog();
//
//        if(responsePojo != null){
//            if(responsePojo.getStatus_code() == 200) {
//                Utilites.showToastMessageShort(MyApplication.mContext, responsePojo.getMessage());
//
//                PreferenceHandler.writeString(MyApplication.mContext, PreferenceHandler.PREF_KEY_FIRST_NAME, responsePojo.getData().getName());
//                PreferenceHandler.writeString(MyApplication.mContext, PreferenceHandler.PREF_KEY_EMAIL, responsePojo.getData().getEmail());
//
//
//               txtName.setText(responsePojo.getData().getName());
//                txtEmail.setText(responsePojo.getData().getEmail());
//
//            }else if(responsePojo.getStatus_code() == 0){
//
//                if(responsePojo.getError() != null){
//
//                    Utilites.showToastMessageShort(MyApplication.mContext, responsePojo.getError().getDescription());
//                }
//
//            }
//
//        }
//    }
//
//    private void handleError(Throwable error) {
//
//        CallProgressWheel.dismissLoadingDialog();
//        Utilites.showToastMessageShort(MyApplication.mContext, Utilites.getErrorMessage(error, MyApplication.mContext));
//    }
}


