package com.brst.aomnia.ui.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.brst.aomnia.R;
import com.brst.aomnia.controller.ApiInterface;
import com.brst.aomnia.controller.RetrofitClient;
import com.brst.aomnia.pojo.ResponsePojo;
import com.brst.aomnia.ui.baseactivities.BaseActivity;
import com.brst.aomnia.utilities.CallProgressWheel;
import com.brst.aomnia.utilities.MyApplication;
import com.brst.aomnia.utilities.MyUtility;
import com.brst.aomnia.utilities.PreferenceHandler;
import com.brst.aomnia.utilities.Utilites;
import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by brst-pc97 on 1/22/18.
 */

public class ForgotPasswordActivity extends BaseActivity {

    private CompositeDisposable mCompositeDisposable;
      Toolbar toolbar;
    TextView  text_toolbar_title;
   EditText  mEdEmail;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        mCompositeDisposable = new CompositeDisposable();
        setUpViews();

        setUpData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCompositeDisposable.dispose();
    }

    @Override
    public void setUpViews() {

        toolbar = findViewById(R.id.toolbar);

        text_toolbar_title = findViewById(R.id.text_toolbar_title);

        mEdEmail = findViewById(R.id.et_email);


    }

    @Override
    public void setUpData() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.back);
        text_toolbar_title.setText("Forgot Password");






    }

    @Override
    public void onClick(View v) {
     switch (v.getId()){

         case R.id.layout_submit:

                if(validation()){
                    if(Utilites.isInternetOn(MyApplication.mContext)){
                        getData();
                    }else {
                        MyUtility.getInstance().showError(this, mEdEmail, "Please check your Internet connection");
                    }

                }
             break;

     }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()){

            case android.R.id.home:
                onBackPressed();
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getData(){

        try {


            try {
                CallProgressWheel.showLoadingDialog(this, "Loading...");
                ApiInterface apiInterface = RetrofitClient.createService(ApiInterface.class);
                JsonObject object = new JsonObject();

                object.addProperty("email",mEdEmail.getText().toString().trim());


                mCompositeDisposable.add(apiInterface.forgotpassword(object)

                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(this::handleResponse,this::handleError));

            }catch (Exception e){
                e.printStackTrace();
                CallProgressWheel.dismissLoadingDialog();
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }



    private void handleResponse(ResponsePojo responsePojo) {

        CallProgressWheel.dismissLoadingDialog();
        if(responsePojo != null){

            if(responsePojo.getStatus_code() == 200) {

                try {


                    AlertDialog.Builder builder = new AlertDialog.Builder(ForgotPasswordActivity.this,
                            R.style.CustomPopUpThemeBlue);
                    builder.setMessage(responsePojo.getMessage());
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            dialog.dismiss();

                            onBackPressed();
                            finish();
                        }
                    });
                    builder.setCancelable(false);
                    builder.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }



            }else if(responsePojo.getError() != null){

                Utilites.showToastMessageShort(MyApplication.mContext, responsePojo.getError().getDescription());
            }

        }
    }

    private void handleError(Throwable error) {

        CallProgressWheel.dismissLoadingDialog();
        Utilites.showToastMessageShort(MyApplication.mContext, Utilites.getErrorMessage(error, MyApplication.mContext));
    }

    private void  setAdapter(){

        try {


        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private boolean validation(){




        if(Utilites.validationInput(mEdEmail)){

            MyUtility.getInstance().showError(this, mEdEmail, "Please enter name");
            return false;
        }
        if(!Utilites.isValidEmailEnter(mEdEmail.getText().toString().trim())){
            MyUtility.getInstance().showError(this, mEdEmail, "Please enter valid email");
            return false;
        }


        return true;
    }
//
}


