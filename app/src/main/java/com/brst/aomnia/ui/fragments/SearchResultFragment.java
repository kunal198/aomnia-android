package com.brst.aomnia.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.brst.aomnia.R;
import com.brst.aomnia.controller.ApiInterface;
import com.brst.aomnia.controller.RetrofitClient;
import com.brst.aomnia.pojo.Android;
import com.brst.aomnia.pojo.CompareIds;
import com.brst.aomnia.pojo.ResponsePojo;
import com.brst.aomnia.pojo.VehicleDatum;
import com.brst.aomnia.ui.activities.HomeActivity;
import com.brst.aomnia.ui.adapter.MakersAdapter;
import com.brst.aomnia.ui.adapter.SearchResultAdapter;
import com.brst.aomnia.utilities.CallProgressWheel;
//import com.brst.aomnia.utilities.EndlessScrollListener;
import com.brst.aomnia.utilities.GlobalBus;
import com.brst.aomnia.utilities.MyApplication;
import com.brst.aomnia.utilities.MyUtility;
import com.brst.aomnia.utilities.PreferenceHandler;
import com.brst.aomnia.utilities.SpacesItemDecoration;
import com.brst.aomnia.utilities.Utilites;
import com.google.gson.JsonObject;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by brst-pc97 on 1/23/18.
 */

public class SearchResultFragment extends Fragment implements View.OnClickListener{
    RecyclerView rv_search_Result;
    SearchResultAdapter adapter;
    LinearLayout mLiCompare;
    private CompositeDisposable mCompositeDisposable;
    public EventBus bus;

    List<VehicleDatum> listVehicleSearch ;
    int counter = 0;
    RecyclerView.LayoutManager mLayoutManager;
    int currentPage = 0;

    public static SearchResultFragment newInstance() {
        SearchResultFragment fragment = new SearchResultFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCompositeDisposable = new CompositeDisposable();
        bus = GlobalBus.getBus();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_search_result, container, false);

        setUpViews(rootView);
        setListner();

        setHasOptionsMenu(false);

        return rootView;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

         mLayoutManager = new LinearLayoutManager(getContext());
        rv_search_Result.setLayoutManager(mLayoutManager);
        int spacingInPixels = 7;

        rv_search_Result.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
//        setAdapter();
    }

    private void setUpViews(View view){
        rv_search_Result=view.findViewById(R.id.rv_search_Result);
        mLiCompare=view.findViewById(R.id.layout_compare);


    }
    private void setListner(){

        mLiCompare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Utilites.showToastMessageShort(MyApplication.mContext,"In Progress...");


                if(counter >=2  ){

                    StringBuilder  builder = getSearchIds();
                    CompareIds obj =    new CompareIds();
                    obj.setCompareIds(builder.toString());

                    bus.postSticky(obj);

                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                    fragmentTransaction.replace(R.id.frame, CompareResultFragment.newInstance(), HomeActivity.CURRENT_TAG);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }else {
                    Utilites.showToastMessageShort(getActivity(), "Please select atleast 2 vehicle");
                }

            }
        });


    }
    private void setAdapter() {

        if (listVehicleSearch == null){
            listVehicleSearch = new ArrayList<>();
        }

        if (adapter == null) {
            adapter = new SearchResultAdapter(getActivity(), listVehicleSearch);
            rv_search_Result.setAdapter(adapter);

            adapter.selectChecxkBox(new SearchResultAdapter.OnSelectVehicle() {
                @Override
                public void getSelectPosition(int pos) {

                    if (counter < 4) {
                        if (!listVehicleSearch.get(pos).isSelected()) {
                            listVehicleSearch.get(pos).setSelected(true);
                            counter++;
                        } else {
                            listVehicleSearch.get(pos).setSelected(false);
                            counter--;
                        }
                    } else {
                        Utilites.showToastMessageShort(getActivity(), "You cannot select more than three items for compare");
                    }
                }
            });


            adapter.ClickListner(new SearchResultAdapter.itemClickListner() {
                @Override
                public void selectItem(int pos) {


                    bus.postSticky(listVehicleSearch.get(pos).getVehicleid());
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                    fragmentTransaction.replace(R.id.frame, CompareSingleVehicleFragment.newInstance(), HomeActivity.CURRENT_TAG);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                }
            });
        }else {
            adapter.notifyDataSetChanged();
            rv_search_Result.setAdapter(adapter);
        }


    }

    @Override
    public void onPause() {
        super.onPause();

        bus.unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        HomeActivity.toolbar.setTitle("Search Results");
        HomeActivity.CURRENT_TAG = "";
        bus.register(this);
        counter= 0;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mCompositeDisposable.dispose();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_search_result,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.actionfilter:
                Utilites.showToastMessageShort(MyApplication.mContext,"In progress...");
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    private void getData(String search){

        try {
            CallProgressWheel.showLoadingDialog(getActivity(), "Loading...");
            ApiInterface apiInterface = RetrofitClient.createService(ApiInterface.class);
            JsonObject object = new JsonObject();
            object.addProperty("searchids",search);

            mCompositeDisposable.add(apiInterface.searchVehicle(object)

                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleResponse,this::handleError));

        }catch (Exception e){
            e.printStackTrace();
            CallProgressWheel.dismissLoadingDialog();
        }


    }

    private void handleResponse(ResponsePojo responsePojo) {

        CallProgressWheel.dismissLoadingDialog();
        if(responsePojo != null){

            if(responsePojo.getStatus_code() == 200) {

                if(responsePojo.getData() != null){

                    if(listVehicleSearch == null){
                        listVehicleSearch = new ArrayList<>();

                    }
                    listVehicleSearch.clear();

                    listVehicleSearch.addAll( responsePojo.getData().getVehicleData());

                    setAdapter();
                }
            }else if(responsePojo.getError() != null){

                Utilites.showToastMessageShort(MyApplication.mContext, responsePojo.getError().getDescription());
            }

        }
    }

    private void handleError(Throwable error) {

        CallProgressWheel.dismissLoadingDialog();
        Utilites.showToastMessageShort(MyApplication.mContext, Utilites.getErrorMessage(error, MyApplication.mContext));
    }


    // event bus to get event string
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void getMessage(String s) {

//        bus.unregister(this);
        if(Utilites.isInternetOn(MyApplication.mContext)){
            getData(s);
        }else {
            MyUtility.getInstance().showError(getContext(), mLiCompare, "Please check your Internet connection");
        }


    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void getId(Integer s) {

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void getId(CompareIds  s) {

    }


    private  StringBuilder getSearchIds(){

        StringBuilder builder = new StringBuilder();

        try {
            for(VehicleDatum datum : listVehicleSearch){

                if(datum.isSelected()){

                    builder.append(datum.getVehicleid());
                    builder.append("^");
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        Log.i("Ids", builder.toString());
        return builder;
    }


}
