package com.brst.aomnia.ui.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brst.aomnia.R;
import com.brst.aomnia.controller.ApiInterface;
import com.brst.aomnia.controller.RetrofitClient;
import com.brst.aomnia.pojo.DataPojo;
import com.brst.aomnia.pojo.Feature;
import com.brst.aomnia.pojo.Overview;
import com.brst.aomnia.pojo.ResponsePojo;
import com.brst.aomnia.pojo.Specification;
import com.brst.aomnia.ui.activities.HomeActivity;
import com.brst.aomnia.ui.adapter.AutoScrollAdaptor;
import com.brst.aomnia.ui.adapter.CompareDescriptionAdapter;
import com.brst.aomnia.ui.adapter.VehicleFeatureAdapter;
import com.brst.aomnia.utilities.AutoScrollViewPager;
import com.brst.aomnia.utilities.CallProgressWheel;
import com.brst.aomnia.utilities.GlobalBus;
import com.brst.aomnia.utilities.MyApplication;
import com.brst.aomnia.utilities.PreferenceHandler;
import com.brst.aomnia.utilities.Utilites;
import com.google.gson.JsonObject;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.brst.aomnia.ui.fragments.FeaturesFragment.pojo;

/**
 * Created by brst-pc97 on 1/23/18.
 */

public class CompareSingleVehicleFragment extends Fragment implements View.OnClickListener{


    AutoScrollViewPager autoScrollViewPagerDetail;
    AppCompatSpinner mAsOverView, mAsFeature, mAsSpecs;
    ArrayAdapter<String> spinnerArrayAdapter,spinnerArrayAdapter1,spinnerArrayAdapter2;
    List<String> listOverView , listFeatures, listSpecs;

    TextView mTvName, mTvPrice, mTvDesc;

    Context context;
    AutoScrollAdaptor adaptorViewPager;
    LinearLayout mLiRequestDemo;
    public EventBus bus;
    private CompositeDisposable mCompositeDisposable;

    TabLayout tabLayoutDetail;
    ViewPager viewPagerDetail;
    DataPojo dataPojo;
    int userId = -1;

    ImageView mIvLeft, mIvRight;

    int itemsize, counter= 0;
    int CarId = -1;


    public static CompareSingleVehicleFragment newInstance() {
        CompareSingleVehicleFragment fragment = new CompareSingleVehicleFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCompositeDisposable = new CompositeDisposable();
        bus = GlobalBus.getBus();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCompositeDisposable.dispose();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_vehicle_detail, container, false);

        setUpViews(rootView);
        setViewPagerAdaptor();
        setListner();

        return rootView;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    private void setUpViews(View view){

        autoScrollViewPagerDetail = view.findViewById(R.id.autoScrollViewPagerDetail);
        mLiRequestDemo = view.findViewById(R.id.layout_request_demo);

        mTvName = view.findViewById(R.id.text_view_vehicle_name);
        mTvPrice =view.findViewById(R.id.text_view_price);
        mTvDesc =view.findViewById(R.id.text_view_vehicle_desc);

        mAsOverView = view.findViewById(R.id.et_choose_overview);
        mAsFeature = view.findViewById(R.id.et_choose_features);
        mAsSpecs = view.findViewById(R.id.et_choose_specification);

        tabLayoutDetail = view.findViewById(R.id.tabLayoutDetail);
        viewPagerDetail = view.findViewById(R.id.viewPagerDetail);
        mIvLeft = view.findViewById(R.id.imageViewLeft);
        mIvRight = view.findViewById(R.id.imageViewRight);



    }
    private void setListner(){



        mLiRequestDemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                userId = PreferenceHandler.readInteger(MyApplication.mContext,PreferenceHandler.PREF_KEY_USER_ID,-1);

               if(userId != -1){

                   PreferenceHandler.writeInteger(getActivity(),PreferenceHandler.PREF_KEY_VEHICLE_ID, CarId );
                   FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                   FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                   fragmentTransaction.replace(R.id.frame, RequestDemoFragment.newInstance(), HomeActivity.CURRENT_TAG);
                   fragmentTransaction.addToBackStack(null);
                   fragmentTransaction.commit();
               }else {

                   try {


                       AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),
                               R.style.CustomPopUpThemeBlue);
                       builder.setMessage("Please login or signup first in order to request demo");

                       builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                           @Override
                           public void onClick(DialogInterface dialog, int which) {
                               dialog.dismiss();
                              PreferenceHandler.writeBoolean(getActivity(),PreferenceHandler.PREF_KEY_ID_REQUEST_USER_DEMO, true);
                               PreferenceHandler.writeInteger(getActivity(),PreferenceHandler.PREF_KEY_VEHICLE_ID, CarId );
                               FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                               FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                               fragmentTransaction.replace(R.id.frame, LoginFragment.newInstance(),HomeActivity.CURRENT_TAG);
                               fragmentTransaction.addToBackStack(null);
                               fragmentTransaction.commit();


                           }
                       });

                       builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

                           @Override
                           public void onClick(DialogInterface dialog, int which) {
                               dialog.dismiss();


                           }
                       });





                       builder.setCancelable(false);
                       builder.show();
                   } catch (Exception e) {
                       e.printStackTrace();
                   }


               }

            }
        });


        mIvLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(counter > 0){
                    --counter;

                    autoScrollViewPagerDetail.setCurrentItem(counter);
                }


            }
        });


        mIvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(counter< itemsize-1){
                    ++counter;
                    autoScrollViewPagerDetail.setCurrentItem(counter);
                }

            }
        });



        Utilites.callBack(new Utilites.callBackFromLoginSignUp() {
            @Override
            public void onClick() {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                try {
                    fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }catch (Exception e){

                }

                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.replace(R.id.frame, RequestDemoFragment.newInstance(), HomeActivity.CURRENT_TAG);
//                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });


    }

    @Override
    public void onPause() {
        super.onPause();

        bus.unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        HomeActivity.toolbar.setTitle("Vehicle detail");
        HomeActivity.CURRENT_TAG = "";
        bus.register(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

        }
    }

    private void setViewPagerAdaptor() {
//        autoScrollViewPagerDetail.startAutoScroll();

        autoScrollViewPagerDetail.setStopScrollWhenTouch(false);

       autoScrollViewPagerDetail.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v, MotionEvent event) {
               return true;
           }
       });
        autoScrollViewPagerDetail.stopAutoScroll();



    }


    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void getMessage(Integer id) {

//        if(dataPojo == null)
            CarId = id;
            getData(id);
    }



    private void getData(int search){

        try {
            CallProgressWheel.showLoadingDialog(getActivity(), "Loading...");
            ApiInterface apiInterface = RetrofitClient.createService(ApiInterface.class);
            JsonObject object = new JsonObject();
            object.addProperty("vehicleid",search);

            mCompositeDisposable.add(apiInterface.getFeature(object)

                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleResponse,this::handleError));

        }catch (Exception e){
            e.printStackTrace();
            CallProgressWheel.dismissLoadingDialog();
        }


    }

    private void handleResponse(ResponsePojo responsePojo) {

        CallProgressWheel.dismissLoadingDialog();
        if(responsePojo != null){

            if(responsePojo.getStatus_code() == 200) {

                if(responsePojo.getData() != null){


                    dataPojo = responsePojo.getData();

                    List<String> imagesList = new ArrayList<>();
                    imagesList =  dataPojo.getGalleryImages();
                    itemsize = imagesList.size();


                    adaptorViewPager = new AutoScrollAdaptor(context, responsePojo.getData().getImage(), imagesList);
                    autoScrollViewPagerDetail.setAdapter(adaptorViewPager);

                    mTvName.setText(dataPojo.getVname());
                    mTvPrice.setText("$"+dataPojo.getPrice()+"");


                    StringBuilder temp = new StringBuilder();

                    if (!dataPojo.getMileage().equalsIgnoreCase("")){
                        temp.append(dataPojo.getMileage());
                    }

                    if (!dataPojo.getFueltype().equalsIgnoreCase("")){
                        temp.append(", ");
                        temp.append(dataPojo.getFueltype());
                    }

                    if (!dataPojo.getDisplacement().equalsIgnoreCase("")){
                        temp.append(", ");
                        temp.append(dataPojo.getDisplacement());
                    }


                    mTvDesc.setText(temp.toString());


                    setDropDownData(dataPojo);

                }
            }else if(responsePojo.getStatus_code() == 400){
                Utilites.showToastMessageShort(MyApplication.mContext, responsePojo.getMessage());
            }

        }
    }

    private void handleError(Throwable error) {

        CallProgressWheel.dismissLoadingDialog();
        Utilites.showToastMessageShort(MyApplication.mContext, Utilites.getErrorMessage(error, MyApplication.mContext));
    }



    private void setDropDownData(DataPojo data){


        setViewPagerAdaptorBottom();
//        if(listOverView == null){
//            listOverView = new ArrayList<>();
//
//        }
//        listOverView.add("--view--");
//        for (Overview object : data.getOverview())
//        {
//            listOverView.add(object.getVname()+"-"+object.getVmake());
//        }
//
//        spinnerArrayAdapter = new ArrayAdapter<String>(
//                getActivity(), R.layout.spinner_item, listOverView);
//        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
//
//        mAsOverView.setAdapter(spinnerArrayAdapter);
//
//        if(listFeatures == null){
//            listFeatures = new ArrayList<>();
//        }
//
//        listFeatures.add("--view--");
//        for (Feature object : data.getFeatures())
//        {
//            listFeatures.add(object.getVtrimlvl());
//        }
//
//
//        spinnerArrayAdapter1 = new ArrayAdapter<String>(
//                getActivity(), R.layout.spinner_item, listFeatures);
//        spinnerArrayAdapter1.setDropDownViewResource(R.layout.spinner_item);
//
//        mAsFeature.setAdapter(spinnerArrayAdapter1);
//
//
//
//        if(listSpecs == null){
//            listSpecs = new ArrayList<>();
//        }
//
//        listSpecs.add("--view--");
//        for (Specification object : data.getSpecifications())
//        {
//            listSpecs.add(object.getPrice()+"");
//        }
//
//        spinnerArrayAdapter2 = new ArrayAdapter<String>(
//                getActivity(), R.layout.spinner_item, listSpecs);
//        spinnerArrayAdapter2.setDropDownViewResource(R.layout.spinner_item);
//
//        mAsSpecs.setAdapter(spinnerArrayAdapter2);

    }


    private void setViewPagerAdaptorBottom() {


        VehicleFeatureAdapter viewPagerAdaptor = new VehicleFeatureAdapter(context, getChildFragmentManager(), 3, dataPojo );

        // Set the adapter onto the view pager
        viewPagerDetail.setAdapter(viewPagerAdaptor);
        tabLayoutDetail.setupWithViewPager(viewPagerDetail);



    }


}


