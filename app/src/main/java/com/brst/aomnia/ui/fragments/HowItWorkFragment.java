package com.brst.aomnia.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.brst.aomnia.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HowItWorkFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HowItWorkFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    RecyclerView mRvCategoryListing;
    EditText mEdSearchCategory;
    Button mBtnSearch;

    SwipeRefreshLayout swipeRefreshLayout;

    View view;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    RecyclerView.LayoutManager mLayoutManager;


    private OnFragmentInteractionListener mListener;

    public HowItWorkFragment() {
        // Required empty public constructor
    }


    public static HowItWorkFragment newInstance() {
        HowItWorkFragment fragment = new HowItWorkFragment();
        Bundle args = new Bundle();


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        if (view != null) {

            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
//        if (view==null) {
            view = inflater.inflate(R.layout.fragment_how_it_works, container, false);

            setUpViews();
            setListner();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    setAdapter();
                }
            }, 200);
            setHasOptionsMenu(true);
//        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onResume() {
        super.onResume();

//        CharityHomeActivity.toolbar.setTitle(CharityHomeActivity.activityTitles[6]);
//        CharityHomeActivity.frameLayoutDonate.setVisibility(View.GONE);
      //  CharityHomeActivity.CURRENT_TAG = "charity";
    }

    private void setUpViews() {




    }

    private void setListner() {

        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {

        try {
            // The number of Columns

        } catch (Exception e)

        {
            e.printStackTrace();
        }


    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inf) {
      //  inf = getActivity().getMenuInflater();
//        inf.inflate(R.menu.all_charity_menu, menu);
        super.onCreateOptionsMenu(menu, inf);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {




        return true;
    }

}
