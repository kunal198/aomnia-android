package com.brst.aomnia.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brst.aomnia.R;
import com.brst.aomnia.pojo.DataPojo;
import com.brst.aomnia.ui.adapter.VehicleOverviewSingleVehicleAdapter;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SpecificationSingleVehicleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SpecificationSingleVehicleFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    View view;

    private OnFragmentInteractionListener mListener;
    RecyclerView mRvOverview;
    VehicleOverviewSingleVehicleAdapter adapter;

    List<String> listOverView;

    static DataPojo pojo;

    TextView mTvLength, mTvWidth, mTvWheelbase, mTvseatingcap, mTvbootspace, mTvFueltank;
    TextView mTvDisplacement , mTvEngineType,  mTvalve2, mTvMileage, mTvFuelType, mTvCylinder;

    public SpecificationSingleVehicleFragment() {
        // Required empty public constructor
    }



    public static SpecificationSingleVehicleFragment newInstance(DataPojo aPojo) {
        SpecificationSingleVehicleFragment fragment = new SpecificationSingleVehicleFragment();
        pojo = aPojo;
        Bundle args = new Bundle();


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

//        if (view != null) {
//
//            ViewGroup parent = (ViewGroup) view.getParent();
//            if (parent != null) {
//                parent.removeView(view);
//            }
//        }
        if (view==null) {
            view = inflater.inflate(R.layout.fragment_specs_single_vehicle, container, false);

            setUpViews();
            setListner();

        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onResume() {
        super.onResume();



    }

    private void setUpViews() {

        try {

            mTvLength = view.findViewById(R.id.text_view_length);
            mTvWidth = view.findViewById(R.id.text_view_width);
            mTvWheelbase = view.findViewById(R.id.text_view_wheelbase);

            mTvseatingcap = view.findViewById(R.id.text_view_setting_cap);
            mTvbootspace = view.findViewById(R.id.text_view_bootspace);
            mTvFueltank = view.findViewById(R.id.text_view_fuel_tank);




            mTvDisplacement = view.findViewById(R.id.text_view_engine_disp);
            mTvalve2 = view.findViewById(R.id.text_view_engine_valve);
            mTvEngineType = view.findViewById(R.id.text_view_engine_type);

            mTvMileage = view.findViewById(R.id.text_view_milege);
            mTvFuelType = view.findViewById(R.id.text_view_fueltype);
            mTvCylinder = view.findViewById(R.id.text_view_cylinder);

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private void setListner() {

        try {

            setAdapter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inf) {
        //  inf = getActivity().getMenuInflater();
//        inf.inflate(R.menu.all_charity_menu, menu);
        super.onCreateOptionsMenu(menu, inf);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {




        return true;
    }


    private void setAdapter(){

        mTvLength.setText(pojo.getSpecificationobj().getDimensions().getLength().getName()+": "+pojo.getSpecificationobj().getDimensions().getLength().getValue());
        mTvWidth.setText(pojo.getSpecificationobj().getDimensions().getWidth().getName()+": "+pojo.getSpecificationobj().getDimensions().getWidth().getValue());
        mTvWheelbase.setText(pojo.getSpecificationobj().getDimensions().getWheelbase().getName()+": "+pojo.getSpecificationobj().getDimensions().getWheelbase().getValue());

        mTvseatingcap.setText(pojo.getSpecificationobj().getCapacity().getSeatingCapacity().getName()+": "+pojo.getSpecificationobj().getCapacity().getSeatingCapacity().getValue());
        mTvbootspace.setText(pojo.getSpecificationobj().getCapacity().getBookspace().getName()+": "+pojo.getSpecificationobj().getCapacity().getBookspace().getValue());
        mTvFueltank.setText(pojo.getSpecificationobj().getCapacity().getFueltank().getName()+": "+pojo.getSpecificationobj().getCapacity().getFueltank().getValue());

        mTvEngineType.setText(pojo.getSpecificationobj().getEngine().getEngineType().getName()+": "+pojo.getSpecificationobj().getEngine().getEngineType().getValue());
        mTvalve2.setText(pojo.getSpecificationobj().getEngine().getValve().getName()+": "+pojo.getSpecificationobj().getEngine().getValve().getValue());
        mTvDisplacement.setText(pojo.getSpecificationobj().getEngine().getDisplacement().getName()+": "+pojo.getSpecificationobj().getEngine().getDisplacement().getValue());

        mTvMileage.setText(pojo.getSpecificationobj().getEngine().getMileage().getName()+": "+pojo.getSpecificationobj().getEngine().getMileage().getValue());


        mTvFuelType.setText(pojo.getSpecificationobj().getEngine().getFueltype().getName()+": "+pojo.getSpecificationobj().getEngine().getFueltype().getValue());

        mTvCylinder.setText(pojo.getSpecificationobj().getEngine().getCylinder().getName()+": "+pojo.getSpecificationobj().getEngine().getCylinder().getValue());

        //        if(listOverView == null){
//            listOverView = new ArrayList<>();
//        }
//
//        for (Specification object : pojo.getSpecifications())
//        {
//            listOverView.add(object.getPrice()+"");
//        }
//
//        adapter = new VehicleOverviewSingleVehicleAdapter(getActivity(), listOverView);
//
//        mRvOverview.setAdapter(adapter);
    }

}
