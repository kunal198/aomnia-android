package com.brst.aomnia.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brst.aomnia.R;
import com.brst.aomnia.pojo.VehicleDatum;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by brst-pc97 on 1/23/18.
 */

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ViewHolder> {


    List<VehicleDatum> mList;
    Context mContext;
    OnSelectVehicle selectionListner;
    itemClickListner itemClickListner;

    public  SearchResultAdapter(Context acontext, List<VehicleDatum> alist){

        mList= alist;
        mContext = acontext;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_search_result, parent, false);
        SearchResultAdapter.ViewHolder viewHolder = new SearchResultAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {

            holder.mTvVehicleName.setText(mList.get(position).getVname());
            StringBuilder temp = new StringBuilder();

              if (!mList.get(position).getMileage().equalsIgnoreCase("")){
                  temp.append(mList.get(position).getMileage());
              }

            if (!mList.get(position).getFueltype().equalsIgnoreCase("")){
                temp.append(", ");
                temp.append(mList.get(position).getFueltype());
            }

            if (!mList.get(position).getDisplacement().equalsIgnoreCase("")){
                temp.append(", ");
                temp.append(mList.get(position).getDisplacement());
            }


            holder.mTvVehicleSpecs.setText(temp.toString());
            holder.mTvVehiclePrice.setText("$"+mList.get(position).getPrice());

            Glide.with(mContext)
                    .load(mList.get(position).getImage())

                    .into(holder.mIvImage);


            holder.mChkBoxSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(selectionListner != null){
                        selectionListner.getSelectPosition(position);
                    }
                }
            });

            holder.mlICarColorContaimer.removeAllViews();

            for (String color : mList.get(position).getColorCode()){

                int colorCode = Color.parseColor(color);

                ImageView image = new ImageView(mContext);

//                image.setLayoutParams(new android.view.ViewGroup.LayoutParams(15,15));

                image.setMaxHeight(15);
                image.setMaxWidth(15);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(15,15);
                lp.setMargins(4, 0, 0, 0);
                image.setLayoutParams(lp);

                image.setColorFilter(colorCode);
                image.setImageResource(R.mipmap.ellipse_white);

                holder.mlICarColorContaimer.addView( image);

            }


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTvVehicleName , mTvVehicleSpecs, mTvVehiclePrice;
        ImageView mIvImage, mIvCarColor;
        AppCompatCheckBox mChkBoxSelect;
        LinearLayout mlICarColorContaimer;
        Button mBtnViewdetails;
        public ViewHolder(View itemView) {
            super(itemView);

            mTvVehicleName = itemView.findViewById(R.id.text_view_vehicle_name);
            mTvVehicleSpecs = itemView.findViewById(R.id.text_view_vehicle_desc);
            mTvVehiclePrice = itemView.findViewById(R.id.text_view_price);
            mlICarColorContaimer = itemView.findViewById(R.id.layout_car_color_container);
            mChkBoxSelect = itemView.findViewById(R.id.check_box_vehicle);
            mIvImage = itemView.findViewById(R.id.car_image);

            mBtnViewdetails = itemView.findViewById(R.id.btn_view_details);
//            mIvCarColor = itemView.findViewById(R.id.image_car_color);

            mBtnViewdetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(itemClickListner != null){
                        itemClickListner.selectItem(getAdapterPosition());
                    }
                }
            });

        }
    }

    public  void selectChecxkBox(OnSelectVehicle listner){

        this.selectionListner = listner;
    }


    public  interface  OnSelectVehicle{

        public  void getSelectPosition(int pos);
    }

    public  void   ClickListner(itemClickListner listner){

        this.itemClickListner = listner;
    }


    public interface  itemClickListner{

        public  void selectItem(int pos);
    }
}
