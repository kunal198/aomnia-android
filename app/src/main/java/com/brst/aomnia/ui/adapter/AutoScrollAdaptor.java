package com.brst.aomnia.ui.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.brst.aomnia.R;
import com.brst.aomnia.utilities.AutoScrollViewPager;
import com.bumptech.glide.Glide;

import java.util.HashMap;
import java.util.List;


/**
 * Created by brst-pc89 on 2/9/17.
 */
public class AutoScrollAdaptor extends PagerAdapter {
    Context context;

    List<String> list;
    LayoutInflater inflater;
    String imageUrl;

    //  int images[]={R.mipmap.ic_module_image,R.mipmap.ic_module_image,R.mipmap.ic_module_image};
    public AutoScrollAdaptor(Context context, String url, List<String> alist) {
        this.context = context;
        this.list = alist;
        this.imageUrl = url;
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);

    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {


         View v=inflater.inflate(R.layout.custom_viewpagerlayout,null);

         ImageView mIvCar = v.findViewById(R.id.image_view_car_scroll);


                Glide.with(context)
                .load(list.get(position))
                .fitCenter()

                .into(mIvCar);


        ((AutoScrollViewPager) container).addView(v, 0);

        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((AutoScrollViewPager) container).removeView((View) object);
    }

}