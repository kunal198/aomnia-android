package com.brst.aomnia.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.brst.aomnia.R;
import com.brst.aomnia.pojo.DataPojo;
import com.brst.aomnia.pojo.Feature;
import com.brst.aomnia.pojo.Overview;
import com.brst.aomnia.ui.adapter.FeaturesAdapter;
import com.brst.aomnia.ui.adapter.VehicleOverviewSingleVehicleAdapter;
import com.brst.aomnia.utilities.SpacesItemDecoration;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FeatureSingleVehicleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FeatureSingleVehicleFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    View view;

    private OnFragmentInteractionListener mListener;
    RecyclerView mRvOverview;
    VehicleOverviewSingleVehicleAdapter adapter;

    List<String> listOverView;

    static DataPojo pojo;



    public FeatureSingleVehicleFragment() {
        // Required empty public constructor
    }



    public static FeatureSingleVehicleFragment newInstance(DataPojo aPojo) {
        FeatureSingleVehicleFragment fragment = new FeatureSingleVehicleFragment();
        pojo = aPojo;
        Bundle args = new Bundle();


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

//        if (view != null) {
//
//            ViewGroup parent = (ViewGroup) view.getParent();
//            if (parent != null) {
//                parent.removeView(view);
//            }
//        }
        if (view==null) {
            view = inflater.inflate(R.layout.fragment_feature_single_vehicle, container, false);

            setUpViews();
            setListner();

        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onResume() {
        super.onResume();



    }

    private void setUpViews() {

        try {

            mRvOverview =  view.findViewById(R.id.rv_single_vehicle);


            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            mRvOverview.setLayoutManager(mLayoutManager);
            int spacingInPixels = 7;


        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private void setListner() {

        try {

            setAdapter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inf) {
        //  inf = getActivity().getMenuInflater();
//        inf.inflate(R.menu.all_charity_menu, menu);
        super.onCreateOptionsMenu(menu, inf);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {




        return true;
    }


    private void setAdapter(){

        if(listOverView == null){
            listOverView = new ArrayList<>();

        }

        for (Feature object : pojo.getFeatures())
        {
            listOverView.add(object.getVpackages());
        }

        adapter = new VehicleOverviewSingleVehicleAdapter(getActivity(), listOverView);

        mRvOverview.setAdapter(adapter);
    }

}
