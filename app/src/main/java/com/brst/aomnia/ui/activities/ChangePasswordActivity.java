package com.brst.aomnia.ui.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.brst.aomnia.R;
import com.brst.aomnia.controller.ApiInterface;
import com.brst.aomnia.controller.RetrofitClient;
import com.brst.aomnia.pojo.ResponsePojo;
import com.brst.aomnia.pojo.UserRequestListing;
import com.brst.aomnia.ui.adapter.AllRequestResultAdapter;
import com.brst.aomnia.ui.baseactivities.BaseActivity;
import com.brst.aomnia.utilities.CallProgressWheel;
import com.brst.aomnia.utilities.MyApplication;
import com.brst.aomnia.utilities.MyUtility;
import com.brst.aomnia.utilities.PreferenceHandler;
import com.brst.aomnia.utilities.SpacesItemDecoration;
import com.brst.aomnia.utilities.Utilites;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by brst-pc97 on 1/22/18.
 */

public class ChangePasswordActivity extends BaseActivity {

    private CompositeDisposable mCompositeDisposable;
      Toolbar toolbar;
    TextView  text_toolbar_title;
   EditText mEdOldPassword, mEdNewPassword, mEdConfirmPassword;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        mCompositeDisposable = new CompositeDisposable();
        setUpViews();

        setUpData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCompositeDisposable.dispose();
    }

    @Override
    public void setUpViews() {

        toolbar = findViewById(R.id.toolbar);

        text_toolbar_title = findViewById(R.id.text_toolbar_title);
        mEdOldPassword = findViewById(R.id.textInputOldPass);
        mEdNewPassword = findViewById(R.id.textInputNewPass);
        mEdConfirmPassword = findViewById(R.id.textInputConfirmPass);

    }

    @Override
    public void setUpData() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.back);
        text_toolbar_title.setText("Change Password");



    }

    @Override
    public void onClick(View v) {
     switch (v.getId()){

         case R.id.textViewForgot:

                if(validation()){
                    if(Utilites.isInternetOn(MyApplication.mContext)){
                        getData();
                    }else {
                        MyUtility.getInstance().showError(this, mEdNewPassword, "Please check your Internet connection");
                    }

                }
             break;

     }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()){

            case android.R.id.home:
                onBackPressed();
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getData(){

        try {


            try {
                CallProgressWheel.showLoadingDialog(this, "Loading...");
                ApiInterface apiInterface = RetrofitClient.createService(ApiInterface.class);
                JsonObject object = new JsonObject();
                object.addProperty("userid",PreferenceHandler.readInteger(this,PreferenceHandler.PREF_KEY_USER_ID, -1));
                object.addProperty("password",mEdNewPassword.getText().toString().trim());
                object.addProperty("oldpassword",mEdOldPassword.getText().toString().trim());

                mCompositeDisposable.add(apiInterface.changepassword(object)

                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(this::handleResponse,this::handleError));

            }catch (Exception e){
                e.printStackTrace();
                CallProgressWheel.dismissLoadingDialog();
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }



    private void handleResponse(ResponsePojo responsePojo) {

        CallProgressWheel.dismissLoadingDialog();
        if(responsePojo != null){

            if(responsePojo.getStatus_code() == 200) {

                try {


                    AlertDialog.Builder builder = new AlertDialog.Builder(ChangePasswordActivity.this,
                            R.style.CustomPopUpThemeBlue);
                    builder.setMessage(responsePojo.getMessage());
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            mEdOldPassword.setText("");
                            mEdConfirmPassword.setText("");
                            mEdConfirmPassword.setText("");
                            onBackPressed();
                            finish();
                        }
                    });
                    builder.setCancelable(false);
                    builder.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }



            }else if(responsePojo.getError() != null){

                Utilites.showToastMessageShort(MyApplication.mContext, responsePojo.getError().getDescription());
            }

        }
    }

    private void handleError(Throwable error) {

        CallProgressWheel.dismissLoadingDialog();
        Utilites.showToastMessageShort(MyApplication.mContext, Utilites.getErrorMessage(error, MyApplication.mContext));
    }

    private void  setAdapter(){

        try {


        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private boolean validation(){




        if(Utilites.validationInput(mEdOldPassword)){

            MyUtility.getInstance().showError(this, mEdOldPassword, "Please enter old password");
            return false;
        }



        if(Utilites.validationInput(mEdNewPassword)){

            MyUtility.getInstance().showError(this, mEdNewPassword, "Please enter new  password");
            return false;
        }



        if(Utilites.validationInput(mEdConfirmPassword)){

            MyUtility.getInstance().showError(this, mEdConfirmPassword, "Please enter confirm  password");
            return false;
        }



        if(!mEdNewPassword.getText().toString().trim().equalsIgnoreCase(mEdConfirmPassword.getText().toString().trim())){

            MyUtility.getInstance().showError(this, mEdNewPassword, "New Password and confirm password not matches");
            return false;
        }


        return true;
    }
//
}


