package com.brst.aomnia.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brst.aomnia.R;
import com.brst.aomnia.pojo.ComparePackage;

import java.util.List;

/**
 * Created by brst-pc97 on 1/23/18.
 */

public class FeaturesAdapter extends RecyclerView.Adapter<FeaturesAdapter.ViewHolder> {



    Context mContext;
    List<ComparePackage> mList;


    public FeaturesAdapter(Context acontext,  List<ComparePackage> alist){

        mList = alist;
        mContext = acontext;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_features_result, parent, false);
        FeaturesAdapter.ViewHolder viewHolder = new FeaturesAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {

            holder.mTvFeatureName.setText(mList.get(position).getName());
            holder.mTvFeatureStatus.setText(mList.get(position).getMode());


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTvFeatureName , mTvFeatureStatus;

        public ViewHolder(View itemView) {
            super(itemView);

            mTvFeatureName = itemView.findViewById(R.id.features_name);
            mTvFeatureStatus = itemView.findViewById(R.id.features_status);
        }
    }





}
