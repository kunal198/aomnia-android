package com.brst.aomnia.utilities;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by brst-pc97 on 12/28/17.
 */

public class MyApplication extends Application {

  public  static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mContext = this.getApplicationContext();
    }
}
