package com.brst.aomnia.utilities;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by brst-pc97 on 11/30/17.
 */

public class GlobalBus {
    private static EventBus sBus;
    public static EventBus getBus() {
        if (sBus == null)
            sBus = EventBus.getDefault();
        return sBus;
    }
}
