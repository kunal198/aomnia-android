package com.brst.aomnia.utilities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.brst.aomnia.R;
import com.brst.aomnia.controller.ApiInterface;
import com.brst.aomnia.controller.RetrofitClient;
import com.brst.aomnia.pojo.ResponsePojo;
import com.brst.aomnia.ui.activities.HomeActivity;
import com.brst.aomnia.ui.fragments.RequestDemoFragment;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;


/**
 * Created by brst-pc97 on 3/24/17.
 */
public class Utilites {
//    PhSMzzJfr8kssCUOA6UBZrO6z
//    PntQn8FghsKQy1oFwVG0CFXXu56xO29Rw6r3r3VUyIUR6Iuo0D
    static callBackFromLoginSignUp handleCallBack;
    static  callBackToSearch handlecallbackSearch;
    private Context context;

    public static boolean isInternetOn(Context context) {

        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);

        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {

            // if connected with internet

//            Toast.makeText(context, " Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {

//            Toast.makeText(context, " Please connect  ", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }


    public static void showLogoutPopUp(final Context mContext) {
        try {

            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle("Logout");
            builder.setMessage("Are you sure you want to logout?");

            builder.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // positive button logic
                               dialog.cancel();
                              PreferenceHandler.writeInteger(MyApplication.mContext,PreferenceHandler.PREF_KEY_USER_ID, -1);
                            ((Activity)mContext).finishAffinity();
                        }
                    });


            builder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();

                            // negative button logic
                        }
                    });
            builder.setCancelable(false);
            AlertDialog dialog = builder.create();
            // display dialog
            dialog.show();
        } catch (Exception e) {
            e.getStackTrace();
        }
    }


    ProgressDialog progressDialog;

    /**
     * @param context
     * @return Returns true if there is network connectivity
     */


    public static boolean isInternetConnection(Context context) {
        try {

            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null)
                    for (int i = 0; i < info.length; i++)
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }


    /**
     * Display Toast Message
     **/
    public static void showToastMessageShort(Context context, String message) {
        Toast.makeText(context.getApplicationContext(), message,
                Toast.LENGTH_SHORT).show();
    }

    /**
     * Display Toast Message
     **/
    public static void showToastMessageLong(Context context, String message) {
        Toast.makeText(context.getApplicationContext(), message,
                Toast.LENGTH_LONG).show();
    }


    public void cancelProgerssDialog() {
        if ((this.progressDialog != null) && (this.progressDialog.isShowing())) {
            this.progressDialog.dismiss();
        }
    }

//

    public static void showErrorDialog(final Activity mContext, String message) {
        try {

            Log.i("message===", message);
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext,
                    R.style.CustomPopUpThemeBlue);
            builder.setMessage(message);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.setCancelable(false);
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static boolean isValidEmailEnter(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";


    /**
     * Validate password with regular expression
     *
     * @param password password for validation
     * @return true valid password, false invalid password
     */
    public static boolean validatePassword(final String password) {

        if (!password.matches(".*[A-Z].*")) return false;

        if (!password.matches(".*\\d.*")) return false;

        return true;

    }


    public static boolean checkPassWordAndConfirmPassword(String password, String confirmPassword) {
        boolean pstatus = false;
        if (confirmPassword != null && password != null) {
            if (password.equals(confirmPassword)) {
                pstatus = true;
            }
        }
        return pstatus;
    }


    /**
     *
     * @param context
     * update user Online offline status.
     */


    /**
     * @param context
     * @return json object of payload
     */
    private static JsonObject payLoad(String onlineStatus, Context context) {
        JsonObject object = null;
        try {
            object = new JsonObject();
            String userId = "", authToken = "";


            object.addProperty("user_id", userId);
            object.addProperty("auth_token", authToken);
            object.addProperty("online_status", onlineStatus);

        } catch (Exception e) {
            e.getStackTrace();
        }
        return object;
    }


    /**
     * @param context
     * @return json object of payload
     */
    private static JsonObject payLoadClearFcmToken(Context context) {
        JsonObject object = null;
        try {
            object = new JsonObject();
            String userId = "", authToken = "";



            object.addProperty("user_id", userId);
            object.addProperty("auth_token", authToken);

        } catch (Exception e) {
            e.getStackTrace();
        }
        return object;
    }


    public static String getDateCurrentTimeZone(long timestamp) {
        try {
            Calendar calendar = Calendar.getInstance(Locale.getDefault());
//            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(timestamp * 1000);
//            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
//            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM, HH:mm a");
//            Date currenTimeZone = (Date) calendar.getTime();
//            return sdf.format(currenTimeZone);

            String date = DateFormat.format("dd-MMM, HH:mm a", calendar).toString();


            return date;
        } catch (Exception e) {
        }
        return "";
    }


    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;


    public static String getTimeAgo(long time, Context ctx) {
//        if (time < 1000000000000L) {
//            // if timestamp given in seconds, convert to millis
//            time *= 1000;
//        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }


//

    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    public static String loadJSONFromAsset(Context context, String filename) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }





    /**
     * Showing google speech input dialog
     */






    public static boolean validationInput(EditText mEdbox){

        try {
            if(mEdbox.getText().toString().trim().length() == 0){

                return true;
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return false;
    }

    public static String getErrorMessage(Throwable throwable, Context mContext) {
        if (throwable instanceof HttpException) {
            // We had non-2XX http error
            return mContext.getString(R.string.error_msg_server);
        }
        if (throwable instanceof IOException) {
            // A network or conversion error happened
            return mContext.getString(R.string.error_msg_network);
        } else {
            // Generic error handling
            return mContext.getString(R.string.error_msg_network_generic);
        }
    }



    public void getProfileData(Activity mCOntext, int userId, CompositeDisposable mCompositeDisposable){

        try {
            CallProgressWheel.showLoadingDialog(MyApplication.mContext, "Loading...");
            ApiInterface apiInterface = RetrofitClient.createService(ApiInterface.class);

            JsonObject object = new JsonObject();
            object.addProperty("userid", userId);


            mCompositeDisposable.add(apiInterface.getProfile(object)

                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleResponse,this::handleError));
        }catch (Exception e){
            e.printStackTrace();
            CallProgressWheel.dismissLoadingDialog();
        }


    }

    public  void handleResponse(ResponsePojo responsePojo) {
        CallProgressWheel.dismissLoadingDialog();

        if(responsePojo != null){
            if(responsePojo.getStatus_code() == 200) {
//                Utilites.showToastMessageShort(MyApplication.mContext, responsePojo.getMessage());

                PreferenceHandler.writeString(MyApplication.mContext, PreferenceHandler.PREF_KEY_FIRST_NAME, responsePojo.getData().getName());
                PreferenceHandler.writeString(MyApplication.mContext, PreferenceHandler.PREF_KEY_EMAIL, responsePojo.getData().getEmail());

                HomeActivity.txtEmail.setText(responsePojo.getData().getEmail());
                HomeActivity.txtName.setText(responsePojo.getData().getName());
                HomeActivity.navigationView.getMenu().getItem(2).setVisible(false);
                HomeActivity.navigationView.getMenu().getItem(3).setVisible(true);
                HomeActivity.navigationView.getMenu().getItem(6).setVisible(true);
                HomeActivity.navigationView.getMenu().getItem(7).setVisible(true);

                if(PreferenceHandler.readBoolean(MyApplication.mContext,PreferenceHandler.PREF_KEY_ID_REQUEST_USER_DEMO,false)){

                    PreferenceHandler.writeBoolean(MyApplication.mContext,PreferenceHandler.PREF_KEY_ID_REQUEST_USER_DEMO,false);

                    if(handleCallBack != null){
                        handleCallBack.onClick();
                    }

                    return;
                }

                if(!PreferenceHandler.readBoolean(MyApplication.mContext,PreferenceHandler.PREF_KEY_ID_REQUEST_USER_DEMO,false)){

                    if (handlecallbackSearch != null){
                        handlecallbackSearch.onClickSearch();
                    }
                }

            }else if(responsePojo.getStatus_code() == 0){

                if(responsePojo.getError() != null){

                    Utilites.showToastMessageShort(MyApplication.mContext, responsePojo.getError().getDescription());


                }

            }

        }
    }

    public  void handleError(Throwable error) {

        CallProgressWheel.dismissLoadingDialog();
        Utilites.showToastMessageShort(MyApplication.mContext, Utilites.getErrorMessage(error, MyApplication.mContext));
    }


    public static void callBack(callBackFromLoginSignUp event){

        handleCallBack = event;
    }

    public static void callBackSearch(callBackToSearch event){

        handlecallbackSearch = event;
    }

    public  interface  callBackFromLoginSignUp{

        public  void onClick();

    }

    public  interface  callBackToSearch{

        public  void onClickSearch();

    }

}
