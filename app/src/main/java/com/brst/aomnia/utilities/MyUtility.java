package com.brst.aomnia.utilities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.brst.aomnia.R;


/**
 * Created by brst-pc89 on 12/28/17.
 */

public class MyUtility {

    ProgressDialog progressDialog;

    static MyUtility instance;

    public static MyUtility getInstance()
    {
        if (instance==null)
        {
            instance=new MyUtility();
        }
        return instance;
    }


    public void showError(Context context, View view, String message) {

        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);

        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        TextView textView = snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }


    public boolean  isNetworkAvailable(Context context)
    {
        ConnectivityManager ConnectionManager=(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=ConnectionManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()==true )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isValidEmail(String target) {

        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public void hideKeypad(Context context)
    {
        InputMethodManager inputManager = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);


        View v = ((Activity) context).getCurrentFocus();

        if (v == null)
            return;
        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


    public void hideProgressDialog()
    {
        if (progressDialog!=null && progressDialog.isShowing())
        {
            progressDialog.hide();
        }
    }

    public void showAlertDialog(final Activity context, String alertmessage_name,final boolean b)

    {
        final AlertDialog alertDialog= new AlertDialog.Builder(context)
                .setMessage(alertmessage_name)
                .setNegativeButton(R.string.alertnegtive_name, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();

                    }
                }).setPositiveButton(R.string.alertpositive_name, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        PreferenceHandler.writeInteger(MyApplication.mContext,PreferenceHandler.PREF_KEY_VEHICLE_ID, -1);
                        context.finishAffinity();
                        dialogInterface.cancel();
                    }
                }).show();

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));

    }
}
