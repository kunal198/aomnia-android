package com.brst.aomnia.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by brst-pc97 on 4/13/17.
 */
public class DataPojo implements Serializable {

    String userid;
    String name;
    String email;


    @SerializedName("trimlvls")
    @Expose
    private List<Maker> trimLvl = null;

    @SerializedName("make")
    @Expose
    private List<Maker> makers = null;
    @SerializedName("packages")
    @Expose
    private List<Package> packages = null;
    @SerializedName("color")
    @Expose
    private List<Color> color = null;
    @SerializedName("models")
    @Expose
    private List<Model> models = null;

    @SerializedName("Vehicle_data")
    @Expose
    private List<VehicleDatum> VehicleData = null;

    @SerializedName("vname")
    @Expose
    private String vname;
    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("gallery_images")
    @Expose
    private List<String> galleryImages = null;

    @SerializedName("price")
    @Expose
    private Integer price;

    @SerializedName("mileage")
    @Expose
    private String mileage;

    @SerializedName("fueltype")
    @Expose
    private String fueltype;

    @SerializedName("displacement")
    @Expose
    private String displacement;

    @SerializedName("overview")
    @Expose
    private List<Overview> overview = null;

    @SerializedName("Vehicle_compare_data")
    @Expose
    private List<VehicleCompareDatum> Vehicle_compare_data = null;



    @SerializedName("features")
    @Expose
    private List<Feature> features = null;
    @SerializedName("specificationsold")
    @Expose
    private List<Specification> specifications = null;


    @SerializedName("user_request_listing")
    @Expose
    private List<UserRequestListing> userRequestListing = null;


    @SerializedName("specifications")
    @Expose
    private Specifications specificationobj;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Maker> getMakers() {
        return makers;
    }

    public void setMakers(List<Maker> makers) {
        this.makers = makers;
    }

    public List<Package> getPackages() {
        return packages;
    }

    public void setPackages(List<Package> packages) {
        this.packages = packages;
    }

    public List<Color> getColor() {
        return color;
    }

    public void setColor(List<Color> color) {
        this.color = color;
    }

    public List<Model> getModels() {
        return models;
    }

    public void setModels(List<Model> models) {
        this.models = models;
    }


    public List<Maker> getTrimLvl() {
        return trimLvl;
    }

    public void setTrimLvl(List<Maker> trimLvl) {
        this.trimLvl = trimLvl;
    }

    public List<VehicleDatum> getVehicleData() {
        return VehicleData;
    }

    public void setVehicleData(List<VehicleDatum> vehicleData) {
        VehicleData = vehicleData;
    }

    public String getVname() {
        return vname;
    }

    public void setVname(String vname) {
        this.vname = vname;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public List<Overview> getOverview() {
        return overview;
    }

    public void setOverview(List<Overview> overview) {
        this.overview = overview;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public List<Specification> getSpecifications() {
        return specifications;
    }

    public void setSpecifications(List<Specification> specifications) {
        this.specifications = specifications;
    }


    public List<VehicleCompareDatum> getVpackages() {
        return Vehicle_compare_data;
    }

    public void setVpackages(List<VehicleCompareDatum> vpackages) {
        this.Vehicle_compare_data = vpackages;
    }

    public List<String> getGalleryImages() {
        return galleryImages;
    }

    public void setGalleryImages(List<String> galleryImages) {
        this.galleryImages = galleryImages;
    }

    public Specifications getSpecificationobj() {
        return specificationobj;
    }

    public void setSpecificationobj(Specifications specificationobj) {
        this.specificationobj = specificationobj;
    }


    public List<UserRequestListing> getUserRequestListing() {
        return userRequestListing;
    }

    public void setUserRequestListing(List<UserRequestListing> userRequestListing) {
        this.userRequestListing = userRequestListing;
    }


    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getFueltype() {
        return fueltype;
    }

    public void setFueltype(String fueltype) {
        this.fueltype = fueltype;
    }

    public String getDisplacement() {
        return displacement;
    }

    public void setDisplacement(String displacement) {
        this.displacement = displacement;
    }
}
