package com.brst.aomnia.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by brst-pc97 on 2/1/18.
 */

public class Capacity implements Serializable {

    @SerializedName("seating_capacity")
    @Expose
    private SeatingCapacity seatingCapacity;
    @SerializedName("bootspace")
    @Expose
    private Bootspace bookspace;
    @SerializedName("fueltank")
    @Expose
    private Fueltank fueltank;

    public SeatingCapacity getSeatingCapacity() {
        return seatingCapacity;
    }

    public void setSeatingCapacity(SeatingCapacity seatingCapacity) {
        this.seatingCapacity = seatingCapacity;
    }

    public Bootspace getBookspace() {
        return bookspace;
    }

    public void setBookspace(Bootspace bookspace) {
        this.bookspace = bookspace;
    }

    public Fueltank getFueltank() {
        return fueltank;
    }

    public void setFueltank(Fueltank fueltank) {
        this.fueltank = fueltank;
    }


}
