package com.brst.aomnia.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by brst-pc97 on 1/30/18.
 */

public class VehicleDatum implements Serializable {

    @SerializedName("vehicleid")
    @Expose
    private Integer vehicleid;
    @SerializedName("vname")
    @Expose
    private String vname;

    @SerializedName("vmodel")
    @Expose
    private String vmodel;
    @SerializedName("vpackages")
    @Expose
    private String vpackages;
    @SerializedName("vmake")
    @Expose
    private String vmake;
    @SerializedName("vtrimlvl")
    @Expose
    private String vtrimlvl;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("mileage")
    @Expose
    private String mileage;
    @SerializedName("fueltype")
    @Expose
    private String fueltype;
    @SerializedName("displacement")
    @Expose
    private String displacement;


    @SerializedName("color_code")
    @Expose
    private List<String> colorCode = null;

    boolean selected;

    public Integer getVehicleid() {
        return vehicleid;
    }

    public void setVehicleid(Integer vehicleid) {
        this.vehicleid = vehicleid;
    }

    public String getVname() {
        return vname;
    }

    public void setVname(String vname) {
        this.vname = vname;
    }



    public String getVmodel() {
        return vmodel;
    }

    public void setVmodel(String vmodel) {
        this.vmodel = vmodel;
    }

    public String getVpackages() {
        return vpackages;
    }

    public void setVpackages(String vpackages) {
        this.vpackages = vpackages;
    }

    public String getVmake() {
        return vmake;
    }

    public void setVmake(String vmake) {
        this.vmake = vmake;
    }

    public String getVtrimlvl() {
        return vtrimlvl;
    }

    public void setVtrimlvl(String vtrimlvl) {
        this.vtrimlvl = vtrimlvl;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public List<String> getColorCode() {
        return colorCode;
    }

    public void setColorCode(List<String> colorCode) {
        this.colorCode = colorCode;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getFueltype() {
        return fueltype;
    }

    public void setFueltype(String fueltype) {
        this.fueltype = fueltype;
    }

    public String getDisplacement() {
        return displacement;
    }

    public void setDisplacement(String displacement) {
        this.displacement = displacement;
    }
}
