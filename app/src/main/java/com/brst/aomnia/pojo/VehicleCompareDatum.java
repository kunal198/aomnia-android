package com.brst.aomnia.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by brst-pc97 on 1/31/18.
 */

public class VehicleCompareDatum implements Serializable {
    @SerializedName("vehicleid")
    @Expose
    private Integer vehicleid;
    @SerializedName("vname")
    @Expose
    private String vname;
    @SerializedName("vmake")
    @Expose
    private String vmake;
    @SerializedName("vmodel")
    @Expose
    private String vmodel;
    @SerializedName("vtrimlvl")
    @Expose
    private String vtrimlvl;
    @SerializedName("compare_packages")
    @Expose
    private List<ComparePackage> comparePackages = null;
    @SerializedName("vcolor")
    @Expose
    private List<String> vcolor = null;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("mileage")
    @Expose
    private String mileage;
    @SerializedName("fueltype")
    @Expose
    private String fueltype;
    @SerializedName("displacement")
    @Expose
    private String displacement;

    @SerializedName("overview")
    @Expose
    private List<Overview> overview = null;

    @SerializedName("specifications")
    @Expose
    private Specifications specificationobj;
    @SerializedName("features")
    @Expose
    private List<Feature> features = null;


    public Integer getVehicleid() {
        return vehicleid;
    }

    public void setVehicleid(Integer vehicleid) {
        this.vehicleid = vehicleid;
    }

    public String getVname() {
        return vname;
    }

    public void setVname(String vname) {
        this.vname = vname;
    }

    public String getVmake() {
        return vmake;
    }

    public void setVmake(String vmake) {
        this.vmake = vmake;
    }

    public String getVmodel() {
        return vmodel;
    }

    public void setVmodel(String vmodel) {
        this.vmodel = vmodel;
    }

    public String getVtrimlvl() {
        return vtrimlvl;
    }

    public void setVtrimlvl(String vtrimlvl) {
        this.vtrimlvl = vtrimlvl;
    }

    public List<ComparePackage> getComparePackages() {
        return comparePackages;
    }

    public void setComparePackages(List<ComparePackage> comparePackages) {
        this.comparePackages = comparePackages;
    }

    public List<String> getVcolor() {
        return vcolor;
    }

    public void setVcolor(List<String> vcolor) {
        this.vcolor = vcolor;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getFueltype() {
        return fueltype;
    }

    public void setFueltype(String fueltype) {
        this.fueltype = fueltype;
    }

    public String getDisplacement() {
        return displacement;
    }

    public void setDisplacement(String displacement) {
        this.displacement = displacement;
    }

    public List<Overview> getOverview() {
        return overview;
    }

    public void setOverview(List<Overview> overview) {
        this.overview = overview;
    }

    public Specifications getSpecificationobj() {
        return specificationobj;
    }

    public void setSpecificationobj(Specifications specificationobj) {
        this.specificationobj = specificationobj;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }
}
