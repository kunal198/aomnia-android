package com.brst.aomnia.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by brst-pc97 on 2/1/18.
 */

public class Engine implements Serializable {

    @SerializedName("engine_type")
    @Expose
    private EngineType engineType;
    @SerializedName("valve")
    @Expose
    private Valve valve;
    @SerializedName("displacement")
    @Expose
    private Displacement displacement;
    @SerializedName("mileage")
    @Expose
    private Mileage mileage;
    @SerializedName("fueltype")
    @Expose
    private Fueltype fueltype;
    @SerializedName("cylinder")
    @Expose
    private Cylinder cylinder;

    public EngineType getEngineType() {
        return engineType;
    }

    public void setEngineType(EngineType engineType) {
        this.engineType = engineType;
    }

    public Valve getValve() {
        return valve;
    }

    public void setValve(Valve valve) {
        this.valve = valve;
    }

    public Displacement getDisplacement() {
        return displacement;
    }

    public void setDisplacement(Displacement displacement) {
        this.displacement = displacement;
    }

    public Mileage getMileage() {
        return mileage;
    }

    public void setMileage(Mileage mileage) {
        this.mileage = mileage;
    }

    public Fueltype getFueltype() {
        return fueltype;
    }

    public void setFueltype(Fueltype fueltype) {
        this.fueltype = fueltype;
    }

    public Cylinder getCylinder() {
        return cylinder;
    }

    public void setCylinder(Cylinder cylinder) {
        this.cylinder = cylinder;
    }
}
