package com.brst.aomnia.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import java.io.Serializable;

/**
 * Created by brst-pc97 on 2/1/18.
 */

public class Specifications implements Serializable {

    @SerializedName("dimensions")
    @Expose
    private Dimensions dimensions;
    @SerializedName("capacity")
    @Expose
    private Capacity capacity;
    @SerializedName("engine")
    @Expose
    private Engine engine;

    public Dimensions getDimensions() {
        return dimensions;
    }

    public void setDimensions(Dimensions dimensions) {
        this.dimensions = dimensions;
    }

    public Capacity getCapacity() {
        return capacity;
    }

    public void setCapacity(Capacity capacity) {
        this.capacity = capacity;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

}
