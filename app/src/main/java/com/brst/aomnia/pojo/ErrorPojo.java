package com.brst.aomnia.pojo;

/**
 * Created by igniva-andriod-11 on 4/5/16.
 */
public class ErrorPojo {

    private String error_code;
    private String error_message;

    public String getError_code() {
        return error_code;
    }

    public String getDescription() {
        return error_message;
    }

}
