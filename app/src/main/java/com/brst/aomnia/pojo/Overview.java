package com.brst.aomnia.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by brst-pc97 on 1/30/18.
 */

public class Overview implements Serializable {

    @SerializedName("vehicleid")
    @Expose
    private Integer vehicleid;
    @SerializedName("vname")
    @Expose
    private String vname;
    @SerializedName("vmake")
    @Expose
    private String vmake;

    @SerializedName("vmodel")
    @Expose
    private String vmodel;

    @SerializedName("displacement")
    @Expose
    private String displacement;
    @SerializedName("mileage")
    @Expose
    private String mileage;
    @SerializedName("fueltype")
    @Expose
    private String fueltype;

    @SerializedName("cylinder")
    @Expose
    private String cylinder;

    @SerializedName("vcolor")
    @Expose
    private List<String> vcolor = null;

    public Integer getVehicleid() {
        return vehicleid;
    }

    public void setVehicleid(Integer vehicleid) {
        this.vehicleid = vehicleid;
    }

    public String getVname() {
        return vname;
    }

    public void setVname(String vname) {
        this.vname = vname;
    }

    public String getVmake() {
        return vmake;
    }

    public void setVmake(String vmake) {
        this.vmake = vmake;
    }

    public String getVmodel() {
        return vmodel;
    }

    public void setVmodel(String vmodel) {
        this.vmodel = vmodel;
    }

    public String getDisplacement() {
        return displacement;
    }

    public void setDisplacement(String displacement) {
        this.displacement = displacement;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getFueltype() {
        return fueltype;
    }

    public void setFueltype(String fueltype) {
        this.fueltype = fueltype;
    }

    public String getCylinder() {
        return cylinder;
    }

    public void setCylinder(String cylinder) {
        this.cylinder = cylinder;
    }

    public List<String> getVcolor() {
        return vcolor;
    }

    public void setVcolor(List<String> vcolor) {
        this.vcolor = vcolor;
    }
}
