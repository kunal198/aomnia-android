package com.brst.aomnia.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.security.SecureRandom;

/**
 * Created by brst-pc97 on 1/30/18.
 */

public class Feature implements Serializable {

    @SerializedName("vmodel")
    @Expose
    private String vmodel;
    @SerializedName("vtrimlvl")
    @Expose
    private String vtrimlvl;
    @SerializedName("vpackages")
    @Expose
    private String vpackages;

    public String getVmodel() {
        return vmodel;
    }

    public void setVmodel(String vmodel) {
        this.vmodel = vmodel;
    }

    public String getVtrimlvl() {
        return vtrimlvl;
    }

    public void setVtrimlvl(String vtrimlvl) {
        this.vtrimlvl = vtrimlvl;
    }

    public String getVpackages() {
        return vpackages;
    }

    public void setVpackages(String vpackages) {
        this.vpackages = vpackages;
    }

}
