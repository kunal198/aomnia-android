package com.brst.aomnia.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by brst-pc97 on 2/1/18.
 */

public class Dimensions  implements Serializable{

    @SerializedName("length")
    @Expose
    private Length length;
    @SerializedName("width")
    @Expose
    private Width width;
    @SerializedName("wheelbase")
    @Expose
    private Wheelbase wheelbase;

    public Length getLength() {
        return length;
    }

    public void setLength(Length length) {
        this.length = length;
    }

    public Width getWidth() {
        return width;
    }

    public void setWidth(Width width) {
        this.width = width;
    }

    public Wheelbase getWheelbase() {
        return wheelbase;
    }

    public void setWheelbase(Wheelbase wheelbase) {
        this.wheelbase = wheelbase;
    }

}
