package com.brst.aomnia.controller;


import android.content.Context;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by cl-macmini-38 on 18/05/16.
 */
public class RetrofitClient {

   private static final int CONNECT_TIMEOUT_MILLIS = 120 * 1000; // 60s
    private static final int READ_TIMEOUT_MILLIS = 120 * 1000; // 60s

    private static OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
    private static Retrofit retrofit = null;

    final static OkHttpClient mokHttpClient = new OkHttpClient.Builder()
            .retryOnConnectionFailure(true)
            .readTimeout(READ_TIMEOUT_MILLIS, TimeUnit.SECONDS)
            .connectTimeout(CONNECT_TIMEOUT_MILLIS, TimeUnit.SECONDS)
            .build();
//
//  .readTimeout(READ_TIMEOUT_MILLIS, TimeUnit.SECONDS)
//            .connectTimeout(CONNECT_TIMEOUT_MILLIS, TimeUnit.SECONDS)


    public static <S> S createService(Class<S> serviceClass) {

        okHttpClient.readTimeout(READ_TIMEOUT_MILLIS, TimeUnit.SECONDS);
        okHttpClient.connectTimeout(CONNECT_TIMEOUT_MILLIS, TimeUnit.SECONDS);

        Retrofit R = getClient();


        return R.create(serviceClass);

    }

    private RetrofitClient(){

    }
    public static synchronized   Retrofit getClient() {

        if (retrofit==null) {

            retrofit = new Retrofit.Builder()

                    .baseUrl(Config.getBaseURL())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(mokHttpClient)
                    .build();
        }
        return retrofit;
    }



}

