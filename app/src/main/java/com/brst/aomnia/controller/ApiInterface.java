package com.brst.aomnia.controller;




import com.brst.aomnia.pojo.Android;
import com.brst.aomnia.pojo.ResponsePojo;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {





    @POST("register")
    Observable<ResponsePojo> register(@Body JsonObject object);

    @POST("login")
    Observable<ResponsePojo> login(@Body JsonObject object);

    @POST("getprofile")
    Observable<ResponsePojo> getProfile(@Body JsonObject object);

    @GET("searchlist")
    Observable<ResponsePojo> getSearchListing();

    @POST("searchvehicle")
    Observable<ResponsePojo> searchVehicle(@Body JsonObject object);


    @POST("vehicledetail")
    Observable<ResponsePojo> getFeature(@Body JsonObject object);

    @POST("vehiclecompare")
    Observable<ResponsePojo> compareVehicles(@Body JsonObject object);

    @retrofit2.http.Multipart
    @POST("requestdemo")
    Observable<ResponsePojo> uploadFile(@retrofit2.http.Part MultipartBody.Part file, @retrofit2.http.Part("userid") RequestBody user_id
    , @retrofit2.http.Part("name") RequestBody name, @retrofit2.http.Part("email") RequestBody email, @retrofit2.http.Part("mobile") RequestBody mobile, @retrofit2.http.Part("comments") RequestBody comments

    , @retrofit2.http.Part("vehicleid") RequestBody vehId
    );


    @POST("requestlist")
    Observable<ResponsePojo> requestList(@Body JsonObject object);

    @POST("changepassword")
    Observable<ResponsePojo> changepassword(@Body JsonObject object);

    @POST("updateprofile")
    Observable<ResponsePojo> updateprofile(@Body JsonObject object);

    @POST("forgotpassword")
    Observable<ResponsePojo> forgotpassword(@Body JsonObject object);
}
